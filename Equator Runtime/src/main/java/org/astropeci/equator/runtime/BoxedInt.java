package org.astropeci.equator.runtime;

public class BoxedInt extends Boxed {

    public final int value;

    public BoxedInt(int value) {
        this.value = value;
    }
}
