/*
A type is essentially property of a value. A value can be a member of a type. A type just defines some operations that
can be done on any member of it. Sometimes you need to prove some operations can be done on anything of a type. A simple
prove statement will suffice. If you need to take a generic type that conforms to this, a constraint can be used.

e.g. Type: Show => Type -> List Char.

Here the ": Show" constrains "Type" so that every member of it is also a show.

It could be re-written as: Show -> List Char.

It is useful for things like: Type: Show => Type -> Type.
It is not equal to: Show -> Show.
*/

type Functor Type {
    let map: NewType => (Type -> NewType) -> Self NewType;
};

type Monad Type {
    prove Functor Type;
    let flatMap: NewType => MonadType: Monad => (Type -> MonadType NewType) -> MonadType NewType;
};

// --------------------------------------------------------

type Semigroupoid Type {
    let partialCombine: Type -> Type -> Option Type;
};

type Category Type {
    prove Semigroupoid Type;
    let identity: Type;
}

type Groupoid Type {
    prove Category Type;
    let inverse: Type -> Type;
}

type Magma Type {
    let combine: Type -> Type -> Type;
};

type QuasiGroup Type {
    prove Magma Type;
};

type Loop Type {
    prove Magma Type;
    let identity: Type;
};

type Semigroup Type {
    prove QuasiGroup Type;
    prove Semigroupoid Type {
        let partialCombine = some << combine;
    };
};

type Monoid Type {
    prove Loop Type;
    prove Category Type {
        let partialCombine = some << combine;
    };
};

type InverseSemigroup Type {
    prove Semigroup Type;
    let inverse: Type -> Type;
};

type Group Type {
    prove InverseSemigroup Type;
    prove Monoid Type;
    prove Groupoid Type {
        let partialCombine = some << combine;
    };
};

type AbelianGroup Type {
    prove Group Type;
};

implicit Type: Num => AbelianGroup Type = {
    let combine = Type.add;
    let identity = 0;
    let inverse = Type.subtract 0;
};

let (+): Type => Magma Type ~> Type -> Type -> Type = magma ~> Magma.combine magma;
overload (+): Type => Semigroupoid Type ~> Type -> Type -> Option Type = semigroupoid ~> Semigroupoid.partialCombine semigroupoid;

let fold: Type => Loop Type ~> Type -> Type -> Type = { list
    ! [] => (Loop Type).identity;
    => (Loop Type).combine (fold (init list)) (tail list);
};