type Option Type {
    prove Monad Type;
};

type Some Type {
    prove Option Type;
};

type None {
    prove Option Nothing;
};

let none = new None {
    let map: None = .;
    let flatMap: None = .;
};

let some = { x: Type => Type -> new Some<Type> {
    let map = (<<) some;
    let flatMap = apply x;
}};