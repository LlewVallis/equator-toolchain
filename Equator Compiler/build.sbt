scalaVersion := "2.12.6"
organization := "org.astropeci.equator"
name := "equator-compiler"
version := "1.0"

// Remove postfix scala versions on artifacts.
crossPaths := false

libraryDependencies += "org.ow2.asm" % "asm" % "6.2"

libraryDependencies += "org.astropeci.equator" % "equator-universal" % "1.0"
libraryDependencies += "org.astropeci.equator" % "equator-parser" % "1.0"