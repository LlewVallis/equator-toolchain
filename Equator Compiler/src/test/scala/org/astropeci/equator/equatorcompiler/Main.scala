package org.astropeci.equator.equatorcompiler

import java.io.{File, IOException}
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes

import scala.collection.JavaConverters._
import scala.io.Source

/**
  * An integration test for the compiler.
  */
object Main extends App {

  val startTime = System.currentTimeMillis()

  val source = Source.fromFile("Example.eq").mkString
  val path = Paths.get("test-output")

  Files.walkFileTree(path, new SimpleFileVisitor[Path]() {
    override def visitFile(file: Path, attr: BasicFileAttributes): FileVisitResult = {
      Files.delete(file)
      FileVisitResult.CONTINUE
    }

    override def postVisitDirectory(dir: Path, e: IOException): FileVisitResult = {
      Files.delete(dir)
      FileVisitResult.CONTINUE
    }
  })

  Files.createDirectories(path)

  val output = EquatorCompiler.compileToDirectory(path, source)

  output.completedPhasesToMillisTaken.foreach {
    case (phase, millis) =>
      println(s"$phase - ${Math.round(millis * 100) / 100d} ms")
  }
  println()

  output match {
    case failure: CompilerOutput.Failure =>
      System.err.println("Error Compiling:")
      failure.cause.printStackTrace()

    case success: CompilerOutput.Success =>
      for (clsName <- Files.list(path).iterator().asScala.map(_.getFileName.toString.replace(".class", ""))) {
        new ProcessBuilder("javap", "-private", "-c", "-s", "-l", clsName)
          .directory(new File("test-output"))
          .inheritIO()
          .start()
          .waitFor()

        println()
      }

      new ProcessBuilder("java", "-cp", "../../Equator Runtime/out/artifacts/equator_runtime_jar/equator-runtime.jar:.",
        "Example")
        .directory(new File("test-output"))
        .inheritIO()
        .start()
        .waitFor()
  }
}
