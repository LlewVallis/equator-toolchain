package org.astropeci.equator.equatorcompiler.bytecode.instruction

import org.objectweb.asm.{MethodVisitor, Opcodes}

/**
  * A BytecodeInstruction that represents a reference local variable load operation.
  */
case class RefLoadInsn(id: Int) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit =
    visitor.visitVarInsn(Opcodes.ALOAD, LocalVariableTypeOffsetter(id, LocalVariableTypeOffsetter.RefType))
}
