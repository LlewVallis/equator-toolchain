package org.astropeci.equator.equatorcompiler.bytecode

/**
  * A NormalBytecodeClass that is built to be a superclass of a boxed function class.
  */
class BoxedFunctionSuperclassBytecodeClass(val descriptor: MethodDescriptor, className: String,
                                           mainClass: MainBytecodeClass, val applyMethodName: String)
  extends NormalBytecodeClass(className, mainClass) {

  superClassName = Some(ConstantNames.BoxedClassName)

  abstractClass = true
  val applyMethod = new BytecodeMethod(applyMethodName, descriptor)
  applyMethod.abstractMethod = true

  methods :+= applyMethod
}