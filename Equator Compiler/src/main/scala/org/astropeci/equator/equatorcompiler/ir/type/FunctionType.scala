package org.astropeci.equator.equatorcompiler.ir.`type`

import org.astropeci.equator.equatorcompiler.ir.{FunctionSignature, IR}

/**
  * A non-passable function.
  */
case class FunctionType(signature: FunctionSignature) extends Type {

  override def applicationReturnType(argument: IR): Type = signature.output
}