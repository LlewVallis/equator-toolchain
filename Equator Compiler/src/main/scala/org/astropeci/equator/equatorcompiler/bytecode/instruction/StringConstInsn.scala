package org.astropeci.equator.equatorcompiler.bytecode.instruction
import org.objectweb.asm.MethodVisitor

/**
  * A BytecodeInstruction that represents a string constant load.
  */
case class StringConstInsn(value: String) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit = visitor.visitLdcInsn(value)
}
