package org.astropeci.equator.equatorcompiler.bytecode.instruction

import org.objectweb.asm.{MethodVisitor, Opcodes}

/**
  * A BytecodeInstruction that represents an integer local variable store operation.
  */
case class IntStoreInsn(id: Int) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit =
    visitor.visitVarInsn(Opcodes.ISTORE, LocalVariableTypeOffsetter(id, LocalVariableTypeOffsetter.IntType))
}
