package org.astropeci.equator.equatorcompiler.bytecode.instruction

import org.objectweb.asm.{MethodVisitor, Opcodes}

/**
  * A BytecodeInstruction that represents a integer local variable load operation.
  */
case class IntLoadInsn(id: Int) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit =
    visitor.visitVarInsn(Opcodes.ILOAD, LocalVariableTypeOffsetter(id, LocalVariableTypeOffsetter.IntType))
}
