package org.astropeci.equator.equatorcompiler.bytecode

import org.astropeci.equator.equatorcompiler.ir.`type`.PassableType

/**
  * Represents the descriptor of a method; it's parameter types and result type. A method descriptor cannot contain
  * non-passable types.
  */
case class MethodDescriptor(inputs: Seq[PassableType], output: PassableType) {

  def toJVMDescriptor(mainClassName: String): String =
    "(" + inputs.map(_.toJVMTypeDescriptor(mainClassName)).mkString + ")" + output.toJVMTypeDescriptor(mainClassName)
}
