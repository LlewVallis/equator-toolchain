package org.astropeci.equator.equatorcompiler.ir

import org.astropeci.equator.equatorcompiler.ir.`type`.Type

/**
  * A signature for a function.
  */
case class FunctionSignature(inputs: Seq[Type], output: Type)
