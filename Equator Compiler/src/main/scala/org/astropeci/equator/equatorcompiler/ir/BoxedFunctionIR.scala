package org.astropeci.equator.equatorcompiler.ir

import org.astropeci.equator.equatorcompiler.bytecode._
import org.astropeci.equator.equatorcompiler.bytecode.instruction.{BytecodeInstruction, DupInsn, InvokeSpecialInsn, NewInsn}
import org.astropeci.equator.equatorcompiler.ir.`type`.{BoxedFunctionType, BoxedType}

/**
  * An IR that represents a function that has been boxed.
  */
case class BoxedFunctionIR(base: FunctionIR) extends PassableIR {

  private val baseBodyPassable = base.body.toPassableIR

  // FIXME this isn't dynamic
  val descriptor = MethodDescriptor(Vector(BoxedType), baseBodyPassable.returnType)

  // TODO add caching for multiple invocations
  override def generateInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction] = {
    val className = graph.generateUniqueClassName(graph.mainClass.className + "_AnonymousFunction_")
    val cls = new NormalBytecodeClass(className, graph.mainClass)
    graph.nonMainClasses :+= cls

    val boxSuperclass =
      BoxedFunctionSuperclassGenerator.generateAndAddIfNotPresent(graph, descriptor, returnType.applyMethodName)
    cls.superClassName = Some(boxSuperclass.className)

    val applyMethod = new BytecodeMethod(returnType.applyMethodName, descriptor)
    applyMethod.instructions = baseBodyPassable.generateInstructions(graph)
    cls.methods :+= applyMethod

    Vector(
      NewInsn(className),
      DupInsn,
      InvokeSpecialInsn(className, "<init>", "()V")
    )
  }

  override def returnType: BoxedFunctionType = BoxedFunctionType(descriptor)
}
