package org.astropeci.equator.equatorcompiler.ir
import org.astropeci.equator.equatorcompiler.ir.`type`.Type

/**
  * An IR that represents an application.
  */
case class ApplicationIR(function: IR, argument: IR) extends IR {

  override def toPassableIR: PassableApplicationIR = PassableApplicationIR(function.toPassableIR, argument.toPassableIR)

  override def returnType: Type = function.returnType.applicationReturnType(argument)
}
