package org.astropeci.equator.equatorcompiler

/**
  * The output of a compilation.
  */
sealed trait CompilerOutput {

  /**
    * @return the successfully completed phases and the amount of time they took to complete in milliseconds
    */
  def completedPhasesToMillisTaken: Map[CompilerPhase, Double]
}

object CompilerOutput {

  trait Success extends CompilerOutput {

    def classNamesToBytecode: Map[String, Array[Byte]]

  }

  trait Failure extends CompilerOutput {

    def failedPhase: CompilerPhase

    def cause: Throwable
  }
}
