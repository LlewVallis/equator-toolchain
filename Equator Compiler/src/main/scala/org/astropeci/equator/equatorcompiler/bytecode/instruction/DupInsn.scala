package org.astropeci.equator.equatorcompiler.bytecode.instruction

import org.objectweb.asm.{MethodVisitor, Opcodes}

/**
  * A BytecodeInstruction that represents a stack duplicate operation.
  */
case object DupInsn extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit = visitor.visitInsn(Opcodes.DUP)
}
