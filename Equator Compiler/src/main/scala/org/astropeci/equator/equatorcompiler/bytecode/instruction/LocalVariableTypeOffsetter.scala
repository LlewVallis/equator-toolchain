package org.astropeci.equator.equatorcompiler.bytecode.instruction

/**
  * Offsets the ids of local variables. An IntStore with the same id as a RefStore will not collide because
  * of this process.
  */
object LocalVariableTypeOffsetter {

  private var typeCount = 0
  sealed trait Type {
    private[LocalVariableTypeOffsetter] val offset = typeCount
    typeCount += 1
  }
  
  // Not case objects so that they are always initialized.
  val RefType: Type = new Type {}
  val IntType: Type = new Type {}

  def apply(id: Int, tpe: Type): Int = {
    id * typeCount + tpe.offset
  }
}
