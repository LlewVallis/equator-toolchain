package org.astropeci.equator.equatorcompiler.optimization

/**
  * An optimizer for a particular data structure.
  */
trait Optimizer[T] extends Function[T, T]
