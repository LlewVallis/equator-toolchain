package org.astropeci.equator.equatorcompiler.bytecode.instruction
import org.objectweb.asm.{MethodVisitor, Opcodes}

/**
  * A BytecodeInstruction that represents a new operation.
  */
case class NewInsn(className: String) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit = visitor.visitTypeInsn(Opcodes.NEW, className)
}
