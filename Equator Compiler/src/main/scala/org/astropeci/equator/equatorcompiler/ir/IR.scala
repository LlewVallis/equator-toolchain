package org.astropeci.equator.equatorcompiler.ir

import org.astropeci.equator.equatorcompiler.ir.`type`.Type

/**
  * An Equator program, or part of an Equator program, as intermediate representation.
  */
trait IR {

  def toPassableIR: PassableIR

  def returnType: Type
}