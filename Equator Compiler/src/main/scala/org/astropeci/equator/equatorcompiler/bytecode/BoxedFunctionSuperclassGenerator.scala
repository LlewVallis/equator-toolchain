package org.astropeci.equator.equatorcompiler.bytecode

import org.astropeci.equator.equatorcompiler.ir.`type`.BoxedFunctionType

/**
  * Generates superclasses for boxed functions.
  */
object BoxedFunctionSuperclassGenerator {

  def generate(descriptor: MethodDescriptor, mainClass: MainBytecodeClass,
               applyMethodName: String): BoxedFunctionSuperclassBytecodeClass = {
    val className = BoxedFunctionType(descriptor).className(mainClass.className)
    new BoxedFunctionSuperclassBytecodeClass(descriptor, className, mainClass, applyMethodName)
  }

  def generateAndAddIfNotPresent(graph: BytecodeGraph,
                                 descriptor: MethodDescriptor,
                                 applyMethodName: String): BoxedFunctionSuperclassBytecodeClass = {
    val existingClass = graph.nonMainClasses.flatMap {
      case cls: BoxedFunctionSuperclassBytecodeClass => Some(cls)
      case _ => None
    }.find(_.descriptor == descriptor)

    existingClass match {
      case Some(cls) => cls
      case None =>
        val cls = generate(descriptor, graph.mainClass, applyMethodName)
        graph.nonMainClasses :+= cls
        cls
    }
  }
}
