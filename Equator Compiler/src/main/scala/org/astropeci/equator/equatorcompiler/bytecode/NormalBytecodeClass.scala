package org.astropeci.equator.equatorcompiler.bytecode

/**
  * A BytecodeClass that is not a main class.
  */
class NormalBytecodeClass(className: String, var mainClass: MainBytecodeClass) extends BytecodeClass(className)