package org.astropeci.equator.equatorcompiler.bytecode

import org.astropeci.equator.equatorcompiler.ir.`type`.PassableType
import org.objectweb.asm.Opcodes._
import org.objectweb.asm.{ClassWriter, MethodVisitor}

/**
  * A BytecodeClass that represents the program's main class.
  *
  * @param className the name of the class to be generated
  */
class MainBytecodeClass(className: String, mainReturnType: PassableType) extends BytecodeClass(className) {

  override def mainClass: MainBytecodeClass = this

  /**
    * The name of the internal variable map to be included in the bytecode.
    */
  var variableMapName: String = "varMap"

  /**
    * The load factor of the variable map, passed to the constructor of HashMap.
    */
  var variableMapLoadFactor: Float = .25f

  /**
    * The initial capacity of the variable map, passed to the constructor of HashMap.
    */
  var variableMapInitialCapacity: Int = ((1 << 16) / variableMapLoadFactor).toInt

  override protected def generateClassBody(writer: ClassWriter): Unit = {
    generateMainMethod(writer)
    super.generateClassBody(writer)
  }

  private def generateMainMethod(writer: ClassWriter): Unit = {
    val visitor = writer.visitMethod(ACC_PUBLIC | ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null)
    visitor.visitCode()

    // Get reference to System.out.
    visitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;")

    // Create a new instance of the main class.
    visitor.visitTypeInsn(NEW, className)
    // Duplicate the reference to the main class.
    visitor.visitInsn(DUP)
    // Call constructor of main class.
    visitor.visitMethodInsn(INVOKESPECIAL, className, "<init>", "()V", false)

    // Stack state is: reference to System.out, reference to main class.

    val mainReturnTypeDescriptor = mainReturnType.toJVMTypeDescriptor(className)
    // Call main method.
    visitor.visitMethodInsn(INVOKEVIRTUAL, className, ConstantNames.EquatorMainMethodName,
      s"()$mainReturnTypeDescriptor", false)

    // Stack state is: reference to System.out, main method return value.

    val printlnArgumentDescriptor =
      if (mainReturnTypeDescriptor.startsWith("L")) "Ljava/lang/Object;" else mainReturnTypeDescriptor
    // Invoke the appropriate System.out.println method.
    visitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", s"($printlnArgumentDescriptor)V", false)

    visitor.visitInsn(RETURN)

    // Can be anything, it is discarded.
    visitor.visitMaxs(0, 0)
    visitor.visitEnd()
  }

  override def generateConstructorBody(visitor: MethodVisitor): Unit = {
    // Load 'this', for the super constructor call and the variable map set.
    visitor.visitVarInsn(ALOAD, 0)
    // Call the super constructor (of Object).
    visitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false)
  }
}