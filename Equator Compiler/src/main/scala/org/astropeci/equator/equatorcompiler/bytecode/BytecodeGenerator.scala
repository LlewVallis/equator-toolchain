package org.astropeci.equator.equatorcompiler.bytecode

/**
  * Generates JVM bytecode from a BytecodeGraph.
  */
object BytecodeGenerator extends Function[BytecodeGraph, Map[String, Array[Byte]]] {

  override def apply(graph: BytecodeGraph): Map[String, Array[Byte]] =
    Map(graph.allClasses.map(cls => (cls.className, cls.toBytecode)): _*)
}
