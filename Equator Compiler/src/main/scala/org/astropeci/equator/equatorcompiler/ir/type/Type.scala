package org.astropeci.equator.equatorcompiler.ir.`type`

import org.astropeci.equator.equatorcompiler.ir.IR

/**
  * A specialized type of value in Equator. There is no guarantee that this type is usable at runtime in
  * bytecode.
  */
trait Type {

  /**
    * @param argument the IR that represents the argument applied to a value of this type
    * @return the resulting type when an argument represented by the given IR is applied to a value of this type
    */
  def applicationReturnType(argument: IR): Type
}