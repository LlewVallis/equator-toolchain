package org.astropeci.equator.equatorcompiler.ir
import org.astropeci.equator.equatorcompiler.bytecode.BytecodeGraph
import org.astropeci.equator.equatorcompiler.bytecode.instruction.BytecodeInstruction
import org.astropeci.equator.equatorcompiler.ir.`type`.PassableType

/**
  * A PassableIR that represents in an application that results in a passable value.
  */
case class PassableApplicationIR(function: PassableIR, argument: PassableIR) extends PassableIR {

  override def generateInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction] =
    function.generateInstructions(graph) ++
      argument.generateInstructions(graph) ++
      function.returnType.generateApplyInstructions(argument.returnType, graph)

  override def returnType: PassableType = function.returnType.applicationReturnType(argument)
}
