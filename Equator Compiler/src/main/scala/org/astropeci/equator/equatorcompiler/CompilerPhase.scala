package org.astropeci.equator.equatorcompiler

/**
  * A phase of compilation.
  */
sealed trait CompilerPhase

object CompilerPhase {

  case object Parsing extends CompilerPhase
  case object ASTOptimization extends CompilerPhase
  case object NonPassableIRGeneration extends CompilerPhase
  case object IROptimization extends CompilerPhase
  case object NonPassableTypeBoxing extends CompilerPhase
  case object BytecodeGraphing extends CompilerPhase
  case object BytecodeGraphOptimization extends CompilerPhase
  case object BytecodeGeneration extends CompilerPhase
}
