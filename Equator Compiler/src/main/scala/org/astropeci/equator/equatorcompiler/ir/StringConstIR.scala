package org.astropeci.equator.equatorcompiler.ir
import org.astropeci.equator.equatorcompiler.bytecode.BytecodeGraph
import org.astropeci.equator.equatorcompiler.bytecode.instruction.{BytecodeInstruction, StringConstInsn}
import org.astropeci.equator.equatorcompiler.ir.`type`.{PassableType, StringType}

/**
  * A PassableIR that represents a string value known at compile time.
  */
case class StringConstIR(value: String) extends PassableIR {

  override def generateInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction] = Vector(
    StringConstInsn(value)
  )

  override def returnType: PassableType = StringType
}
