package org.astropeci.equator.equatorcompiler.bytecode.instruction

import org.objectweb.asm.{MethodVisitor, Opcodes}

/**
  * A BytecodeInstruction that represents an invoke special operation.
  */
case class InvokeSpecialInsn(className: String, methodName: String,
                             methodDescriptor: String) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit =
    visitor.visitMethodInsn(Opcodes.INVOKESPECIAL, className, methodName, methodDescriptor, false)
}