package org.astropeci.equator.equatorcompiler.bytecode

/**
  * An immediate representation tightly related to JVM bytecode. A BytecodeGraph exists primarily to allow for low
  * level optimization techniques. A BytecodeGraph can be built in an asynchronous manner, but it may not be modified
  * while bytecode is being generated.
  */
class BytecodeGraph(var mainClass: MainBytecodeClass) {

  var nonMainClasses: Seq[NormalBytecodeClass] = Vector()

  def allClasses: Seq[BytecodeClass] = nonMainClasses :+ mainClass

  private var uuid = BigInt(0)

  def generateUniqueClassName(classNameBase: String): String = {
    val res = classNameBase + uuid.toString(16)
    uuid += 1
    res
  }
}
