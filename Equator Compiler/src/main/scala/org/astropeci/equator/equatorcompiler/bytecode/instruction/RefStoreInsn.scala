package org.astropeci.equator.equatorcompiler.bytecode.instruction
import org.objectweb.asm.{MethodVisitor, Opcodes}

/**
  * A BytecodeInstruction that represents a reference local variable store operation.
  */
case class RefStoreInsn(id: Int) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit =
    visitor.visitVarInsn(Opcodes.ASTORE, LocalVariableTypeOffsetter(id, LocalVariableTypeOffsetter.RefType))
}
