package org.astropeci.equator.equatorcompiler.ir

import java.util.regex.Pattern

import org.astropeci.equator.equatorcompiler.ir.`type`.IntType
import org.astropeci.equator.equatoruniversal._

/**
  * Generates non-passable IR from an AST.
  */
object NonPassableIRGenerator extends Function[AST, IR] {

  private val DecIntegerMatcher = Pattern.compile("(\\+|-)?[0-9]+").matcher("")
  private val OctIntegerMatcher = Pattern.compile("(\\+|-)?0o[0-7]+").matcher("")
  private val HexIntegerMatcher = Pattern.compile("(\\+|-)?0x[0-9A-F]+").matcher("")
  private val BinIntegerMatcher = Pattern.compile("(\\+|-)?0b[01]+").matcher("")

  override def apply(ast: AST): IR =
    genIntLiteral(ast)
      .orElse(genStringLiteral(ast))
      .orElse(genFunction(ast))
      .orElse(genApplication(ast))
      .get

  private def genIntLiteral(ast: AST): Option[IR] =
    try {
      ast match {
        case IdentifierAST(id, _) if DecIntegerMatcher.reset(id).matches() =>
          Some(IntConstIR(Integer.parseInt(id)))
        case IdentifierAST(id, _) if OctIntegerMatcher.reset(id).matches() =>
          Some(IntConstIR(Integer.parseInt(id.replace("0o", ""), 8)))
        case IdentifierAST(id, _) if HexIntegerMatcher.reset(id).matches() =>
          Some(IntConstIR(Integer.parseInt(id.replace("0x", ""), 16)))
        case IdentifierAST(id, _) if BinIntegerMatcher.reset(id).matches() =>
          Some(IntConstIR(Integer.parseInt(id.replace("0b", ""), 2)))
        case _ => None
      }
    } catch {
      // For integer overflows.
      case _: NumberFormatException =>
        None
    }

  private def genStringLiteral(ast: AST): Option[IR] = ast match {
    case StringLiteralAST(content, _) => Some(StringConstIR(content))
    case _ => None
  }

  private def genFunction(ast: AST): Option[IR] = ast match {
    case FunctionAST(body, _) =>
      val bodyIR = apply(body)
      // FIXME stop assuming IntType for argument
      Some(FunctionIR(FunctionSignature(Vector(IntType), bodyIR.returnType), bodyIR))
    case _ => None
  }

  private def genApplication(ast: AST): Option[IR] = ast match {
    case ApplicationAST(function, argument, _) =>
      val functionIR = apply(function)
      val argumentIR = apply(argument)
      Some(ApplicationIR(functionIR, argumentIR))
    case _ => None
  }
}
