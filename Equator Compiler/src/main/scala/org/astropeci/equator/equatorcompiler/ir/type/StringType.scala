package org.astropeci.equator.equatorcompiler.ir.`type`
import org.astropeci.equator.equatorcompiler.bytecode.BytecodeGraph
import org.astropeci.equator.equatorcompiler.bytecode.instruction.BytecodeInstruction
import org.astropeci.equator.equatorcompiler.ir.IR
import org.objectweb.asm.Opcodes

/**
  * A string.
  */
case object StringType extends PassableType {

  override def uuid: String = "String"

  override def toASMReturnOpcode: Int = Opcodes.ARETURN

  override def toJVMTypeDescriptor(mainClassName: String): String = "Ljava/lang/String;"

  override def generateBoxingInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction] = ???

  override def applicationReturnType(argument: IR): PassableType = ???

  override def generateApplyInstructions(tpe: PassableType, graph: BytecodeGraph): Seq[BytecodeInstruction] = ???
}
