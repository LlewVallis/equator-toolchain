package org.astropeci.equator.equatorcompiler.bytecode

object ConstantNames {

  val EquatorMainMethodName = "eqMain"

  val BoxedClassName = "org/astropeci/equator/runtime/Boxed"

  val BoxedIntClassName = "org/astropeci/equator/runtime/BoxedInt"
}
