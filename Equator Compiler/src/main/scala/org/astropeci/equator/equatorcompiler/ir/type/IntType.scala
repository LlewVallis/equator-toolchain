package org.astropeci.equator.equatorcompiler.ir.`type`

import org.astropeci.equator.equatorcompiler.bytecode.{BytecodeGraph, ConstantNames}
import org.astropeci.equator.equatorcompiler.bytecode.instruction._
import org.astropeci.equator.equatorcompiler.ir.IR
import org.objectweb.asm.Opcodes

/**
  * A 32-bit signed integer.
  */
case object IntType extends PassableType {

  override def toASMReturnOpcode: Int = Opcodes.IRETURN

  override def toJVMTypeDescriptor(mainClassName: String): String = "I"

  override def generateBoxingInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction] = Vector(
    IntStoreInsn(0),
    NewInsn(ConstantNames.BoxedIntClassName),
    DupInsn,
    IntLoadInsn(0),
    InvokeSpecialInsn(ConstantNames.BoxedIntClassName, "<init>", "(I)V")
  )

  override def uuid: String = "Int"

  override def applicationReturnType(argument: IR): PassableType = ???

  override def generateApplyInstructions(tpe: PassableType, graph: BytecodeGraph): Seq[BytecodeInstruction] = ???
}
