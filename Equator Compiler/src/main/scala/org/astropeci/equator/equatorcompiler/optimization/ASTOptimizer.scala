package org.astropeci.equator.equatorcompiler.optimization

import org.astropeci.equator.equatoruniversal.AST

/**
  * An optimizer for Equator ASTs.
  */
object ASTOptimizer extends Optimizer[AST] {

  override def apply(ast: AST): AST = ast
}
