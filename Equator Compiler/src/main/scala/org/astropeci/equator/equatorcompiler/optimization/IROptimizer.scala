package org.astropeci.equator.equatorcompiler.optimization

import org.astropeci.equator.equatorcompiler.ir.IR

/**
  * An optimizer for Equator ASTs.
  */
object IROptimizer extends Optimizer[IR] {

  override def apply(ir: IR): IR = ir
}
