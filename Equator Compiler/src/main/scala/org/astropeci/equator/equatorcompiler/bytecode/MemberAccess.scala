package org.astropeci.equator.equatorcompiler.bytecode

import org.objectweb.asm.Opcodes

/**
  * The access level of a method or field.
  */
sealed trait MemberAccess {

  /**
    * @return the ASM access code of this access level
    */
  def toASMAccess: Int
}

case object PublicAccess extends MemberAccess {

  override def toASMAccess: Int = Opcodes.ACC_PUBLIC
}

case object PrivateAccess extends MemberAccess {

  override def toASMAccess: Int = Opcodes.ACC_PRIVATE
}

case object ProtectedAccess extends MemberAccess {

  override def toASMAccess: Int = Opcodes.ACC_PROTECTED
}
case object PackageAccess extends MemberAccess {

  override def toASMAccess: Int = 0
}