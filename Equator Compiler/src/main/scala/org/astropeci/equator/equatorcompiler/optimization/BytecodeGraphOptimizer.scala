package org.astropeci.equator.equatorcompiler.optimization

import org.astropeci.equator.equatorcompiler.bytecode.BytecodeGraph

/**
  * An optimizer for Equator ASTs.
  */
object BytecodeGraphOptimizer extends Optimizer[BytecodeGraph] {

  override def apply(graph: BytecodeGraph): BytecodeGraph = graph
}
