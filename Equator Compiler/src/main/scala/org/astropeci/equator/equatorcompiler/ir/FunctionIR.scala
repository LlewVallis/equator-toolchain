package org.astropeci.equator.equatorcompiler.ir
import org.astropeci.equator.equatorcompiler.ir.`type`.{FunctionType, Type}

/**
  * An IR that represents a function.
  */
case class FunctionIR(signature: FunctionSignature, body: IR) extends IR {

  override def toPassableIR: PassableIR = BoxedFunctionIR(this)

  override def returnType: Type = FunctionType(signature)
}
