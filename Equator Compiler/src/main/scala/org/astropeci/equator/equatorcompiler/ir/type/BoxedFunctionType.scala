package org.astropeci.equator.equatorcompiler.ir.`type`
import org.astropeci.equator.equatorcompiler.bytecode.instruction.{BytecodeInstruction, InvokeVirtualInsn}
import org.astropeci.equator.equatorcompiler.bytecode.{BytecodeGraph, MethodDescriptor}
import org.astropeci.equator.equatorcompiler.ir.IR
import org.objectweb.asm.Opcodes

/**
  * A passable function.
  */
case class BoxedFunctionType(descriptor: MethodDescriptor) extends PassableType {

  override def toASMReturnOpcode: Int = Opcodes.ARETURN

  override def toJVMTypeDescriptor(mainClassName: String): String = "L" + className(mainClassName) + ";"

  override def generateBoxingInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction] = Vector()

  override def uuid: String = "BoxedFunctionType_" + uniqueDescriptorString

  /**
    * Returns the postfix that can be added to the main class's name in order to create the class name that corresponds
    * to the common superclass of boxed functions of this type.
    *
    * @return the postfix that can be added to the main class's name in order to create this instance's class name
    */
  def classNamePostfix: String =  "_BoxedFunctionSuperclass_" + uniqueDescriptorString

  def className(mainClassName: String): String = mainClassName + classNamePostfix

  def applyMethodName: String = "boxedFunctionApply"

  private def uniqueDescriptorString: String =
    descriptor.inputs.length + "_" +
      descriptor.inputs.map(_.uuid).mkString("_") + "_" +
      descriptor.output.uuid

  override def applicationReturnType(argument: IR): PassableType = descriptor.output

  override def generateApplyInstructions(tpe: PassableType, graph: BytecodeGraph): Seq[BytecodeInstruction] = {
    val mainClassName = graph.mainClass.className
    tpe.generateBoxingInstructions(graph) :+
      InvokeVirtualInsn(className(mainClassName), applyMethodName, descriptor.toJVMDescriptor(mainClassName))
  }
}