package org.astropeci.equator.equatorcompiler.bytecode.instruction

import org.objectweb.asm.MethodVisitor

/**
  * An analogue for a JVM bytecode instruction.
  */
trait BytecodeInstruction {

  def visitInstruction(visitor: MethodVisitor): Unit
}