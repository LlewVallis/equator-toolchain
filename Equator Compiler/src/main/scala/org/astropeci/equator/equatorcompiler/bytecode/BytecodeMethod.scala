package org.astropeci.equator.equatorcompiler.bytecode

import org.astropeci.equator.equatorcompiler.bytecode.instruction.BytecodeInstruction
import org.objectweb.asm.MethodVisitor

/**
  * An analogue for a method in JVM bytecode.
  */
class BytecodeMethod(var name: String, var descriptor: MethodDescriptor) {

  var access: MemberAccess = PublicAccess

  var instructions: Seq[BytecodeInstruction] = Vector()

  var abstractMethod: Boolean = false

  def visitInstructions(visitor: MethodVisitor): Unit = instructions.foreach(_.visitInstruction(visitor))
}
