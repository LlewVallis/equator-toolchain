package org.astropeci.equator.equatorcompiler.bytecode.instruction
import org.objectweb.asm.{MethodVisitor, Opcodes}

/**
  * A BytecodeInstruction that represents an invoke static operation.
  */
case class InvokeStaticInsn(className: String, methodName: String,
                            methodDescriptor: String) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit =
    visitor.visitMethodInsn(Opcodes.INVOKESTATIC, className, methodName, methodDescriptor, false)
}
