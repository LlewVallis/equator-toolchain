package org.astropeci.equator.equatorcompiler.ir.`type`

import org.astropeci.equator.equatorcompiler.bytecode.BytecodeGraph
import org.astropeci.equator.equatorcompiler.bytecode.instruction.BytecodeInstruction
import org.astropeci.equator.equatorcompiler.ir.IR
import org.objectweb.asm.Opcodes

/**
  * The type of a generalized boxed value.
  */
case object BoxedType extends PassableType {

  override def toASMReturnOpcode: Int = Opcodes.ARETURN

  override def toJVMTypeDescriptor(mainClassName: String): String = "Lorg/astropeci/equator/runtime/Boxed;"

  override def generateBoxingInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction] = Vector()

  override def uuid: String = "BoxedType"

  override def applicationReturnType(argument: IR): PassableType = BoxedType

  override def generateApplyInstructions(tpe: PassableType, graph: BytecodeGraph): Seq[BytecodeInstruction] = ???
}
