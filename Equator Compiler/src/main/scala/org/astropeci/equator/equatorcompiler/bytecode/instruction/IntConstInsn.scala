package org.astropeci.equator.equatorcompiler.bytecode.instruction
import org.objectweb.asm.MethodVisitor

/**
  * A BytecodeInstruction that represents an integer constant load.
  */
case class IntConstInsn(value: Int) extends BytecodeInstruction {

  override def visitInstruction(visitor: MethodVisitor): Unit = visitor.visitLdcInsn(value)
}
