package org.astropeci.equator.equatorcompiler.ir

import org.astropeci.equator.equatorcompiler.bytecode.BytecodeGraph
import org.astropeci.equator.equatorcompiler.bytecode.instruction.BytecodeInstruction
import org.astropeci.equator.equatorcompiler.ir.`type`.PassableType

/**
  * An IR in which all types are passable.
  */
trait PassableIR extends IR {

  /**
    * Generates BytecodeInstructions which can be validly converted into JVM bytecode.
    *
    * @param graph the graph that the instructions will be placed in, provided so additional methods can be added by the
    *              implementation
    * @return the BytecodeInstructions
    */
  def generateInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction]

  override def returnType: PassableType

  override def toPassableIR: this.type = this
}
