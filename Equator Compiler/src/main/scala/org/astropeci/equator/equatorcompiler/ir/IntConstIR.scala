package org.astropeci.equator.equatorcompiler.ir

import org.astropeci.equator.equatorcompiler.bytecode.BytecodeGraph
import org.astropeci.equator.equatorcompiler.bytecode.instruction.{BytecodeInstruction, IntConstInsn}
import org.astropeci.equator.equatorcompiler.ir.`type`.{IntType, PassableType}

/**
  * A PassableIR that represents a 32-bit signed integer literal that is known at compile time.
  */
case class IntConstIR(value: Int) extends PassableIR {

  override def generateInstructions(graph: BytecodeGraph): Seq[BytecodeInstruction] = Vector(IntConstInsn(value))

  override def returnType: PassableType = IntType
}
