package org.astropeci.equator.equatorcompiler.ir

/**
  * Generates a PassableIR from an IR.
  */
object NonPassableTypeBoxer extends Function[IR, PassableIR] {

  override def apply(ir: IR): PassableIR = ir match {
    case ir: PassableIR => ir
    case _ => ir.toPassableIR
  }
}
