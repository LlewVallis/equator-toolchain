package org.astropeci.equator.equatorcompiler.bytecode

import org.astropeci.equator.equatorcompiler.ir.PassableIR

/**
  * Generates a BytecodeGraph from a PassableIR.
  */
object BytecodeGrapher extends Function[PassableIR, BytecodeGraph] {

  override def apply(ir: PassableIR): BytecodeGraph = {
    val mainClass = new MainBytecodeClass("Example", ir.returnType)
    val graph = new BytecodeGraph(mainClass)
    val mainMethod = new BytecodeMethod(ConstantNames.EquatorMainMethodName, MethodDescriptor(Vector(), ir.returnType))

    mainMethod.instructions = ir.generateInstructions(graph)

    mainClass.methods :+= mainMethod
    graph
  }
}
