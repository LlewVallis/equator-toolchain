package org.astropeci.equator.equatorcompiler

import java.nio.file.{Files, Path, Paths}

import org.astropeci.equator.equatorcompiler.bytecode.{BytecodeGenerator, BytecodeGrapher}
import org.astropeci.equator.equatorcompiler.ir.{NonPassableIRGenerator, NonPassableTypeBoxer}
import org.astropeci.equator.equatorcompiler.optimization.{ASTOptimizer, BytecodeGraphOptimizer, IROptimizer}
import org.astropeci.equator.equatorparser.EquatorParser

import scala.util.{Failure, Success, Try}

/**
  * A compiler that transforms Equator source code to JVM class files.
  */
object EquatorCompiler {

  def compile(source: String): CompilerOutput = {
    val nanosToMillis = 1 / 1e6
    var timedPhases = Map[CompilerPhase, Double]()
    def putDonePhase(phase: CompilerPhase, startNanos: Long, endNanos: Long): Unit =
      timedPhases += ((phase, (endNanos - startNanos) * nanosToMillis))


    val start = System.nanoTime()
    EquatorParser.parse(source) match {
      case Success(ast) =>
        val parseEnd = System.nanoTime()
        putDonePhase(CompilerPhase.Parsing, start, parseEnd)

        val optimizedAST = ASTOptimizer(ast)
        val astOptimizeEnd = System.nanoTime()
        putDonePhase(CompilerPhase.ASTOptimization, parseEnd, astOptimizeEnd)

        val nonPassableIR = NonPassableIRGenerator(optimizedAST)
        val nonPassableIREnd = System.nanoTime()
        putDonePhase(CompilerPhase.NonPassableIRGeneration, astOptimizeEnd, nonPassableIREnd)

        val optimizedIR = IROptimizer(nonPassableIR)
        val irOptimizeEnd = System.nanoTime()
        putDonePhase(CompilerPhase.IROptimization, nonPassableIREnd, irOptimizeEnd)

        val boxedIR = NonPassableTypeBoxer(optimizedIR)
        val irBoxEnd = System.nanoTime()
        putDonePhase(CompilerPhase.NonPassableTypeBoxing, irOptimizeEnd, irBoxEnd)

        val bytecodeGraph = BytecodeGrapher(boxedIR)
        val bytecodeGraphEnd = System.nanoTime()
        putDonePhase(CompilerPhase.BytecodeGraphing, irBoxEnd, bytecodeGraphEnd)

        val optimizedBytecodeGraph = BytecodeGraphOptimizer(bytecodeGraph)
        val bytecodeGraphOptimizeEnd = System.nanoTime()
        putDonePhase(CompilerPhase.BytecodeGraphOptimization, bytecodeGraphEnd, bytecodeGraphOptimizeEnd)

        val generatedBytecode = BytecodeGenerator(optimizedBytecodeGraph)
        val bytecodeGenerationEnd = System.nanoTime()
        putDonePhase(CompilerPhase.BytecodeGeneration, bytecodeGraphOptimizeEnd, bytecodeGenerationEnd)

        new CompilerOutput.Success {
          override def classNamesToBytecode: Map[String, Array[Byte]] = generatedBytecode
          override def completedPhasesToMillisTaken: Map[CompilerPhase, Double] = timedPhases
        }
      case Failure(e) => new CompilerOutput.Failure {
        override def failedPhase: CompilerPhase = CompilerPhase.Parsing
        override def cause: Throwable = e
        override def completedPhasesToMillisTaken: Map[CompilerPhase, Double] = Map()
      }
    }
  }

  def compileToDirectory(directory: Path, source: String): CompilerOutput =
    compile(source) match {
      case success: CompilerOutput.Success =>
        success.classNamesToBytecode.foreach {
          case (className, bytecode) =>
            val path = directory.resolve(className + ".class")
            Files.write(path, bytecode)
        }

        success

      case failure: CompilerOutput.Failure => failure
    }

  def compileToDirectory(directory: String, source: String): CompilerOutput =
    compileToDirectory(Paths.get(directory), source)
}
