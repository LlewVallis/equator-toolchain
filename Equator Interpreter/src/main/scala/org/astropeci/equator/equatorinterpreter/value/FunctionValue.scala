package org.astropeci.equator.equatorinterpreter.value

import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.meta.{DefaultFunctionMeta, MetaValue}
import org.astropeci.equator.equatoruniversal.AST

class FunctionValue(val body: AST, val meta: Value) extends Value {

  def this(body: AST) = this(body, DefaultFunctionMeta)

  override def apply(arg: Value)(implicit interpreter: EquatorInterpreter): Value = {
    interpreter.rebindIdentifier("_", arg) {
      interpreter.valueOfAST(body)
    }
  }

  override def metaMap(meta: Value): FunctionValue = new FunctionValue(body, meta)
}
