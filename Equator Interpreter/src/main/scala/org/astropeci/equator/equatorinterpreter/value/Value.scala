package org.astropeci.equator.equatorinterpreter.value

import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.meta.MetaValue

import scala.language.existentials

trait Value {

  def apply(arg: Value)(implicit interpreter: EquatorInterpreter): Value

  def meta: Value

  def metaMap(meta: Value): (T) forSome { type T >: this.type <: Value }

  def toString(implicit interpreter: EquatorInterpreter): String = meta match {
    case meta: MetaValue => meta.strValue
    case _ => meta(new StringValue(".str")).toString
  }
}