package org.astropeci.equator.equatorinterpreter.value
import org.astropeci.equator.equatorinterpreter.EquatorInterpreter

class ReboundValue(val base: Value, val reboundSymbol: String, val reboundValue: Value) extends Value {

  override def apply(arg: Value)(implicit interpreter: EquatorInterpreter): Value =
    interpreter.rebindIdentifier(reboundSymbol, reboundValue) {
      new ReboundValue(base.apply(arg), reboundSymbol, reboundValue)
    }

  override def meta: Value = base.meta

  override def metaMap(meta: Value): ReboundValue = new ReboundValue(base.metaMap(meta), reboundSymbol, reboundValue)
}
