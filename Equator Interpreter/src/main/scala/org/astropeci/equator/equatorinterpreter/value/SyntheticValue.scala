package org.astropeci.equator.equatorinterpreter.value
import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.meta.DefaultFunctionMeta

import scala.language.implicitConversions

class SyntheticValue(_apply: (Value, EquatorInterpreter) => Value,
                     override val meta: Value = DefaultFunctionMeta) extends Value {

  override def apply(arg: Value)(implicit interpreter: EquatorInterpreter): Value = _apply(arg, interpreter)

  override def metaMap(meta: Value): SyntheticValue = new SyntheticValue(_apply, meta)
}

object SyntheticValue {

  def apply(f: (Value, EquatorInterpreter) => Value): SyntheticValue = new SyntheticValue(f)

  def apply(f: Value => Value): SyntheticValue = new SyntheticValue((value, _) => f(value))
}
