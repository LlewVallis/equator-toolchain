package org.astropeci.equator.equatorinterpreter.meta

object DefaultFunctionMeta extends StandardMetaValue(Vector(), _ => "function")
