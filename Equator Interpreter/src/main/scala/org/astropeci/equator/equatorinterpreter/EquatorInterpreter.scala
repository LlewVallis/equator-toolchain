package org.astropeci.equator.equatorinterpreter

import org.astropeci.equator.equatorinterpreter.value._
import org.astropeci.equator.equatorparser.EquatorParser
import org.astropeci.equator.equatoruniversal._

import scala.collection.mutable.{HashMap => MutableHashMap}
import scala.io.Source
import scala.util.{Failure, Success}

class EquatorInterpreter private (ast: AST) {

  import EquatorInterpreter._

  private val symbolTable: MutableHashMap[String, Seq[Value]] = MutableHashMap[String, Seq[Value]]()

  private def stackOfSymbol(symbol: String): Seq[Value] =
    symbolTable.getOrElseUpdate(symbol, defaultValueOfIdentifier(symbol) :: Nil)

  /** Gets the value of a standard identifier in the current context. */
  def valueOfIdentifier(symbol: String): Value = stackOfSymbol(symbol).head

  def rebindIdentifier[T](symbol: String, value: Value)(f: => T): T = {
    symbolTable.put(symbol, value +: stackOfSymbol(symbol))
    val res = f
    symbolTable.put(symbol, stackOfSymbol(symbol).tail)
    res
  }

  def run(): Value = valueOfAST(ast)

  /** Gets the resulting value of evaluations an AST in the current context. */
  def valueOfAST(ast: AST): Value = {
    ast.toCoreAST match {
      case application: ApplicationAST => valueOfAST(application.function).apply(valueOfAST(application.argument))(this)
      case function: FunctionAST => new FunctionValue(function.body)
      case identifier: IdentifierAST => valueOfIdentifier(identifier.identifier)
      case list: ListAST => new StandardListValue(list.elements.map(valueOfAST).toVector)
      case string: StringLiteralAST => new StringValue(string.content)
    }
  }
}

object EquatorInterpreter {

  private val DoubleRegex = "[-+]?[0-9]*.?[0-9]+".r

  //fixme remove
  var source: String = _

  def main(args: Array[String]): Unit = {
    val path = "example.eq"
    source = Source.fromFile(path).mkString
    EquatorParser.parse(source) match {
      case Success(ast) =>
        println(ast.toSourceCode)
        println()

        implicit val interpreter: EquatorInterpreter = new EquatorInterpreter(ast)
        val value = interpreter.run()

        println(value.toString)
      case Failure(e) =>
        System.err.println("Failed to parse file: ")
        e.printStackTrace()
    }
  }

  /** Gets the default value of a standard identifier. No user code can alter this. */
  def defaultValueOfIdentifier(identifier: String): Value = identifier match {
    case DoubleRegex(_*) => new NumberValue(identifier.toDouble)
    case "meta" => SyntheticValue(value => value.meta)
    case "infix_=" =>
      SyntheticValue(symbol =>
        SyntheticValue(value =>
          SyntheticValue(base =>
            symbol match {
              case symbol: StringValue => new ReboundValue(base, symbol.content, value)
              case _ => ???
            }
          )
        )
      )
    case _ => new UndefinedValue(identifier)
  }
}
