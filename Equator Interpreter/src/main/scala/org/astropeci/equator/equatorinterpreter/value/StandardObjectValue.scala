package org.astropeci.equator.equatorinterpreter.value
import org.astropeci.equator.equatorinterpreter.EquatorInterpreter

class StandardObjectValue(val _members: Map[String, Value], val meta: Value) extends ObjectValue {

  override type SelfType = StandardObjectValue

  override protected def members(implicit interpreter: EquatorInterpreter): Map[String, Value] = _members

  override def metaMap(meta: Value): SelfType = new StandardObjectValue(_members, meta)
}
