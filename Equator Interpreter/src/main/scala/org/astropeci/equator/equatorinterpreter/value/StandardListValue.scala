package org.astropeci.equator.equatorinterpreter.value

import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.meta.DefaultStandardListMeta

class StandardListValue(val elements: Vector[Value], val meta: Value) extends ListValue {

  override type SelfType = StandardListValue

  def this(elements: Vector[Value]) = this(elements, new DefaultStandardListMeta(
    interpreter => StandardListValue.makeString(elements)(interpreter) ))

  override def at(index: Int): Value = if (elements.isDefinedAt(index)) {
    elements(index)
  } else {
    ???
  }

  override def size: Int = elements.size

  override def tail: StandardListValue = new StandardListValue(elements.tail, meta)

  override def init: StandardListValue = new StandardListValue(elements.init, meta)

  override def metaMap(meta: Value): SelfType = new StandardListValue(elements, meta)
}

object StandardListValue {

  private def makeString(elements: Seq[Value])(implicit interpreter: EquatorInterpreter): String =
    elements.map(_.toString).mkString("[", ", ", "]")
}