package org.astropeci.equator.equatorinterpreter.value

import org.astropeci.equator.equatorinterpreter.EquatorInterpreter

import scala.collection.immutable.HashMap

trait ListValue extends ObjectValue {

  override type SelfType >: this.type <: ListValue

  def at(index: Int): Value

  def size: Int

  def tail: SelfType

  def init: SelfType

  override protected def members(implicit interpreter: EquatorInterpreter): Map[String, Value] = HashMap(
    ".at" -> SyntheticValue(_ match {
      case num: NumberValue =>
        val index = num.value.toInt
        if (index < 0) new UndefinedValue("negative index")
        else if (index >= size) new UndefinedValue("index too large")
        else at(index)
      case _ => ???
    }),
    ".size" -> new NumberValue(size),
    ".tail" -> (if (size == 0) new UndefinedValue("tail of empty list") else tail),
    ".init" -> (if (size == 0) new UndefinedValue("init of empty list") else init)
  )
}
