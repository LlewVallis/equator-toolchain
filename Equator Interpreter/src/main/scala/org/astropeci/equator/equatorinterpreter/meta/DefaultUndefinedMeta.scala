package org.astropeci.equator.equatorinterpreter.meta

class DefaultUndefinedMeta(message: String) extends StandardMetaValue(Vector("undefined"), _ => s"undefined($message)")
