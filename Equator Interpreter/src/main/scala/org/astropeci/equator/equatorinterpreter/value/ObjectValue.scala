package org.astropeci.equator.equatorinterpreter.value

import org.astropeci.equator.equatorinterpreter.EquatorInterpreter

trait ObjectValue extends Value {

  type SelfType >: this.type <: ObjectValue

  override def apply(arg: Value)(implicit interpreter: EquatorInterpreter): Value = arg match {
    case arg: StringValue => members.getOrElse(arg.content, new UndefinedValue(arg.content))
    case _ => println(this, arg); ???
  }

  override def metaMap(meta: Value): SelfType

  protected def members(implicit interpreter: EquatorInterpreter): Map[String, Value]
}