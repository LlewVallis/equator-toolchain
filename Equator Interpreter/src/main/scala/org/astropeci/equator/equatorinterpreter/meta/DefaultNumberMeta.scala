package org.astropeci.equator.equatorinterpreter.meta

class DefaultNumberMeta(value: Double) extends StandardMetaValue(Vector("number", "object"), _ => value.toString)
