package org.astropeci.equator.equatorinterpreter.meta

import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.value._

import scala.collection.immutable.HashMap

trait MetaValue extends ObjectValue {

  override type SelfType = MetaValue

  protected def additionalMembers: Map[String, Value]

  def typeStrings(implicit interpreter: EquatorInterpreter): Seq[String]

  def strValue(implicit interpreter: EquatorInterpreter): String

  override protected def members(implicit interpreter: EquatorInterpreter): Map[String, Value] = HashMap(
    ".type" -> new StandardListValue(typeStrings.map(new StringValue(_)).toVector),
    ".str" -> new StringValue(strValue)
  ) ++ additionalMembers

  override def meta: Value = DefaultMetaMeta
}
