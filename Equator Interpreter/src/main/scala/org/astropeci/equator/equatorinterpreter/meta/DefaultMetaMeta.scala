package org.astropeci.equator.equatorinterpreter.meta

object DefaultMetaMeta extends StandardMetaValue(Vector("meta", "object"), _ => "meta")