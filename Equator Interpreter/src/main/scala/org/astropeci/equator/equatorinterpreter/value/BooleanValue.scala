package org.astropeci.equator.equatorinterpreter.value
import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.meta.DefaultBooleanMeta

import scala.collection.immutable.HashMap

class BooleanValue(val value: Boolean, override val meta: Value) extends ObjectValue {

  override type SelfType = BooleanValue

  def this(value: Boolean) = this(value, new DefaultBooleanMeta(value))

  override protected def members(implicit interpreter: EquatorInterpreter): Map[String, Value] = HashMap(
    ".branch" -> SyntheticValue(a => SyntheticValue(b =>
      if (value) a else b
    ))
  )

  override def metaMap(meta: Value): BooleanValue = new BooleanValue(value, meta)
}
