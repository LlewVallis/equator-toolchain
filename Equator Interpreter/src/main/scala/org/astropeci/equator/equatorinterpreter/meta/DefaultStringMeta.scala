package org.astropeci.equator.equatorinterpreter.meta

class DefaultStringMeta(content: String) extends StandardMetaValue(Vector("string", "list", "object"), _ => content)
