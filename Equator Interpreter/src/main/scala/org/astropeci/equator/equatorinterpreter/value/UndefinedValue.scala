package org.astropeci.equator.equatorinterpreter.value
import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.meta.DefaultUndefinedMeta

class UndefinedValue(val message: String, val meta: Value) extends Value {

  def this(message: String) = this(message, new DefaultUndefinedMeta(message))

  override def apply(arg: Value)(implicit interpreter: EquatorInterpreter): Value = this

  override def metaMap(meta: Value): UndefinedValue = new UndefinedValue(message, meta)
}
