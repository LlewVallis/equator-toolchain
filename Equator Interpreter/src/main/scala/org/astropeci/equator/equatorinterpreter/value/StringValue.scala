package org.astropeci.equator.equatorinterpreter.value
import org.astropeci.equator.equatorinterpreter.meta.DefaultStringMeta

class StringValue(val content: String, val meta: Value) extends ListValue {

  override type SelfType = StringValue

  def this(content: String) = this(content, new DefaultStringMeta(content))

  override def at(index: Int): Value = new StringValue(content.charAt(index).toString, meta)

  override def size: Int = content.length

  override def tail: StringValue = new StringValue(content.substring(1), meta)

  override def init: StringValue = new StringValue(content.substring(0, size - 1), meta)

  override def metaMap(meta: Value): StringValue = new StringValue(content, meta)
}
