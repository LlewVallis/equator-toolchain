package org.astropeci.equator.equatorinterpreter.meta

class DefaultBooleanMeta(value: Boolean) extends StandardMetaValue(Vector("boolean", "object"), _ => value.toString)
