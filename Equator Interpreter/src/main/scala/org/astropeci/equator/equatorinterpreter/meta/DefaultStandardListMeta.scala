package org.astropeci.equator.equatorinterpreter.meta

import org.astropeci.equator.equatorinterpreter.EquatorInterpreter

class DefaultStandardListMeta(strValueProducer: EquatorInterpreter => String) extends StandardMetaValue(
  Vector("list", "object"), strValueProducer)