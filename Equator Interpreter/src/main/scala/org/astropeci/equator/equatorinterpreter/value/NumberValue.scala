package org.astropeci.equator.equatorinterpreter.value
import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.meta.DefaultNumberMeta

import scala.collection.immutable.HashMap

class NumberValue(val value: Double, override val meta: Value) extends ObjectValue {

  override type SelfType = NumberValue

  def this(value: Double) = this(value, new DefaultNumberMeta(value))

  override protected def members(implicit interpreter: EquatorInterpreter): Map[String, Value] = HashMap(
    ".add" -> SyntheticValue(_ match {
      case that: NumberValue => new NumberValue(this.value + that.value)
      case _ => ???
    }),
    ".subtract" -> SyntheticValue(_ match {
      case that: NumberValue => new NumberValue(this.value - that.value)
      case _ => ???
    }),
    ".multiply" -> SyntheticValue(_ match {
      case that: NumberValue => new NumberValue(this.value * that.value)
      case _ => ???
    }),
    ".divide" -> SyntheticValue(_ match {
      case that: NumberValue => new NumberValue(this.value / that.value)
      case _ => ???
    }),
    ".greater" -> SyntheticValue(_ match {
      case that: NumberValue => new BooleanValue(this.value > that.value)
      case _ => ???
    }),
    ".lesser" -> SyntheticValue(_ match {
      case that: NumberValue => new BooleanValue(this.value < that.value)
      case _ => ???
    }),
    ".equals" -> SyntheticValue(_ match {
      case that: NumberValue => new BooleanValue(this.value == that.value)
      case _ => ???
    })
  )

  override def metaMap(meta: Value): NumberValue = new NumberValue(value, meta)
}
