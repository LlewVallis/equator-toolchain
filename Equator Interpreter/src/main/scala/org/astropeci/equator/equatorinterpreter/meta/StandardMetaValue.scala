package org.astropeci.equator.equatorinterpreter.meta
import org.astropeci.equator.equatorinterpreter.EquatorInterpreter
import org.astropeci.equator.equatorinterpreter.value.Value

import scala.collection.immutable.HashMap

class StandardMetaValue(_typeStrings: Seq[String], strValueProducer: EquatorInterpreter => String,
                        override val meta: Value) extends MetaValue {

  def this(typeStrings: Seq[String], strValueProducer: EquatorInterpreter => String) =
    this(typeStrings, strValueProducer, DefaultMetaMeta)

  override protected def additionalMembers: Map[String, Value] = HashMap()

  override def typeStrings(implicit interpreter: EquatorInterpreter): Seq[String] = _typeStrings

  override def strValue(implicit interpreter: EquatorInterpreter): String = strValueProducer(interpreter)

  override def metaMap(meta: Value): MetaValue = new StandardMetaValue(_typeStrings, strValueProducer, meta)
}
