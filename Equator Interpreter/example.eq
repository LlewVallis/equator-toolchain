@"infix_->" = {
    @`argNames = _;

    {
        @`f = _;

        argNames .size .equals 1 .branch
            {
                @argNames .at 0 = _;
                f undefined
            }
            {
                @argNames .at 0 = _;
                argNames.tail -> f
            }
    }
};

42