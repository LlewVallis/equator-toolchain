# Equator Toolchain

**A set of utilities for the Equator programming language.**

## Overview

This project contains modules for parsing Equator programs into ASTs and compiling them into JVM bytecode.

## Building

All the contined project folders are independent SBT projects.

### Complications and Known Issues

#### All

When importing into IntelliJ, a null pointer exception may be thrown by the IDE. To fix this, run SBT from the command line in the desired module to generate necessary files.

#### Parser

When importing into IntelliJ, the IDE may try to compile generated Antlr4 sources twice. To fix this, remove '*target/scala-2.12/src_managed/main*' as a source directory under *project structure* > *modules* > *sources*.

When importing into IntelliJ, the IDE may try to fail due to not being able to resolve sources and JavaDocs for the sbt-antlr4 plugin. To fix this configure IntelliJ to not download library sources under *preferences* > *build, execution and deployment* > *build tools* > *sbt*.

## Informal Language Spec

### File

A file contains a single extended expression.

### Extended Expression

An extended expression is any amount of variable definitions followed by
a simple expression.

E.g.
```
let x = 4;
let y = 4;
x + y
```

### Simple Expression

#### Integer Literal

Can be 1, 2, 3, -42, etc. It must be in range for a signed 32-bit integer.

Can be a hex literal. I.e. 16xCAFEBABE. Must be uppercase except for the `x`.

The `16` can be changed to any number to customize radix. A radix less than 2 or greater than 16 is invalid.

#### List Literal

Are some expression in square parens with commas.

E.g.
```
[1, 2, 3, "Bob"]
```

If there are any double commas, any items seperated by single commas get sublisted.

E.g.
```
[42, 10,, "some", "text",, 1,, 2,, 3] == [[42, 10], ["some", "text"], [1], [2], [3]]
```

Also applies recursively for any amount of back to back commas.

#### String Literal

Some text seperated by double quotes. Can be multiline.

#### Function Literal

Can be an extended expression surrounded with curly braces, or an arrow
function.

E.g.
```
{ _ + 1 }

{
    let x = 3;
    x + 1
}

{ a b c => a + b + c }

{ a b c =>
    let x = 42;
    a + b + c + x
}
```

Argument types are inferred. E.g. In `{ x => f x }`, `x` is inferred to be the same type as whatever `f` expects. In `{ x => f x + g x }` x is inferred to be the intersection of the types expected of `f` and `g`.

Guards can be used in arrow functions.

E.g.
```
{ n : n == 0 => 1
    : n == 1 => 1
             => . n-1 + . n-2
}

{ _ : _ == 0 => 1
    : _ == 1 => 1
             => . _-1 + . _-2
}
```

The period refers to the current function and an underscore refers to
the function argument if it is unnamed.

#### Application

Application can be infix, like in `x + y` or can be prefix, like in `a b c`.

If a symbolic identifier is used, `x + y` is equal to `+ x y`.

#### Proofs

An