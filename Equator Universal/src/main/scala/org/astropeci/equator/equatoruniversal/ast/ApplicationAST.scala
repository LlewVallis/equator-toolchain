package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal.SourceSpan
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents a standard application.
  */
case class ApplicationAST(function: AST, argument: AST, override val sourceSpan: Option[SourceSpan]) extends CoreAST {

  override protected def mapChildren(f: AST => AST): AST = ApplicationAST(f(function), f(argument), sourceSpan)

  override def children: Seq[AST] = Vector(function, argument)

  override def toSourceCode(compact: Boolean): String = if (compact) {
    s"(${function.toSourceCode(true)})(${argument.toSourceCode(true)})"
  } else {
    val leftStr = StringUtil.indent(function.toSourceCode(false))
    val rightStr = StringUtil.indent(argument.toSourceCode(false))
    s"(\n$leftStr\n) (\n$rightStr\n)"
  }
}
