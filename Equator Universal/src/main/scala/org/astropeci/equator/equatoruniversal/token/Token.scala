package org.astropeci.equator.equatoruniversal.token

import org.astropeci.equator.equatoruniversal.SourceSpan
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * A token extracted from Equator source code.
  */
trait Token {

  def tokenType: TokenType

  def text: String

  def sourceSpan: SourceSpan

  override def toString: String = s"$tokenType('${StringUtil.escape(text)}', $sourceSpan)]"
}

object Token {

  def apply(tokenType: TokenType, text: String, sourceSpan: SourceSpan): Token = {
    // Bypass shadowing.
    val theTokenType = tokenType
    val theText = text
    val theSourceSpan = sourceSpan

    new Token {

      override def tokenType: TokenType = theTokenType

      override def text: String = theText

      override def sourceSpan: SourceSpan = theSourceSpan
    }
  }
}