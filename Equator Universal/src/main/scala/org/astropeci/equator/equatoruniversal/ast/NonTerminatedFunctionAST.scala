package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal.SourceSpan
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents a non terminated function.
  */
case class NonTerminatedFunctionAST(body: AST, override val sourceSpan: Option[SourceSpan]) extends AST {

  override def mapChildren(f: AST => AST): AST = NonTerminatedFunctionAST(f(body), sourceSpan)

  override def children: Seq[AST] = Vector(body)

  override def toSourceCode(compact: Boolean): String = if (compact) {
    s";${body.toSourceCode(true)}"
  } else {
    val applicantStr = StringUtil.indent(body.toSourceCode(false))
    s";\n$applicantStr"
  }

  override def toCoreAST: CoreAST = FunctionAST(body, sourceSpan)
}
