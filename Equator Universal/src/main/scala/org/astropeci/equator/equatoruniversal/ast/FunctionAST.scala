package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal.SourceSpan
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents a terminated function.
  */
case class FunctionAST(body: AST, override val sourceSpan: Option[SourceSpan]) extends CoreAST {

  override def mapChildren(f: AST => AST): AST = FunctionAST(f(body), sourceSpan)

  override def children: Seq[AST] = Vector(body)

  override def toSourceCode(compact: Boolean): String = if (compact) {
    s"{${body.toSourceCode(true)}}"
  } else {
    val bodyStr = StringUtil.indent(body.toSourceCode(false))
    s"{\n$bodyStr\n}"
  }
}
