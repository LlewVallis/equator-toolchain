package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal.SourceSpan
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents a list.
  */
case class ListAST(elements: Seq[AST], override val sourceSpan: Option[SourceSpan]) extends CoreAST {

  override def mapChildren(f: AST => AST): AST = ListAST(elements.map(f), sourceSpan)

  override def children: Seq[AST] = elements

  override def toSourceCode(compact: Boolean): String = if (compact) {
    elements.map(_.toSourceCode(true)).mkString("[", ",", "]")
  } else {
    elements.map(elem => StringUtil.indent(elem.toSourceCode(false))).mkString("[\n", ",\n", "\n]")
  }

  override def toString: String = s"$productPrefix(${elements.mkString(",")})"
}
