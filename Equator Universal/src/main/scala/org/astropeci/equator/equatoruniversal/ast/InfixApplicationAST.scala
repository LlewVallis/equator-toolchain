package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal._
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents a symbolic infix application.
  */
case class InfixApplicationAST(left: AST, identifier: String, right: AST,
                               override val sourceSpan: Option[SourceSpan]) extends AST {

  override protected def mapChildren(f: AST => AST): AST =
    InfixApplicationAST(f(left), identifier, f(right), sourceSpan)

  override def children: Seq[AST] = Vector(left, right)

  override def toSourceCode(compact: Boolean): String = if (compact) {
    s"(${left.toSourceCode(true)})$identifier(${right.toSourceCode(true)})"
  } else {
    val leftStr = StringUtil.indent(left.toSourceCode(false))
    val rightStr = StringUtil.indent(right.toSourceCode(false))
    s"(\n$leftStr\n) $identifier (\n$rightStr\n)"
  }

  override def toCoreAST: CoreAST =
    ApplicationAST(
      ApplicationAST(
        IdentifierAST.ofInfix(identifier, sourceSpan), left, sourceSpan
      ),
      right, sourceSpan
    )
}
