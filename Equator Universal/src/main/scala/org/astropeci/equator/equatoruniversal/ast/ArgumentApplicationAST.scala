package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal._
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents an argument application.
  */
case class ArgumentApplicationAST(applicant: AST, override val sourceSpan: Option[SourceSpan]) extends AST {

  override def mapChildren(f: AST => AST): AST = ArgumentApplicationAST(f(applicant), sourceSpan)

  override def children: Seq[AST] = Vector(applicant)

  override def toSourceCode(compact: Boolean): String = if (compact) {
    s"@${applicant.toSourceCode(true)}"
  } else {
    val applicantStr = StringUtil.indent(applicant.toSourceCode(false))
    s"@\n$applicantStr"
  }

  override def toCoreAST: CoreAST = ApplicationAST(applicant, IdentifierAST("_", sourceSpan), sourceSpan)
}
