package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal.SourceSpan
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents a standard string literal.
  */
case class StringLiteralAST(content: String, override val sourceSpan: Option[SourceSpan]) extends CoreAST {

  override def mapChildren(f: AST => AST): AST = this

  override def children: Seq[AST] = Vector()

  override def toSourceCode(compact: Boolean): String = '"' + StringUtil.escape(content) + '"'
}
