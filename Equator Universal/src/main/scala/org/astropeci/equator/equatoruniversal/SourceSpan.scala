package org.astropeci.equator.equatoruniversal

/**
  * A span of source code. Line starts are one indexed, but column starts are zero indexed. Ending lines are inclusive,
  * but ending columns are exclusive.
  */
case class SourceSpan(startLine: Int, startColumn: Int, endLine: Int, endColumn: Int) {

  def |(that: SourceSpan): SourceSpan = {
    val lowest = SourceSpan.lowest(this, that)
    val highest = SourceSpan.highest(this, that)

    SourceSpan(
      lowest.startLine,
      lowest.startColumn,
      highest.endLine,
      highest.endColumn
    )
  }

  def &(that: SourceSpan): SourceSpan = {
    val lowest = SourceSpan.lowest(this, that)
    val highest = SourceSpan.highest(this, that)

    SourceSpan(
      highest.startLine,
      highest.startColumn,
      lowest.endLine,
      lowest.endColumn
    )
  }

  def getSnippet(source: String): String = {
    var column = 0
    var line = 0
    var lastCharWasCarriageReturn = false

    val builder = new StringBuilder()

    for (char <- source) {
      char match {
        case '\n' if lastCharWasCarriageReturn =>
          lastCharWasCarriageReturn = false
        case '\n' if !lastCharWasCarriageReturn =>
          column = 0
          line += 1
          lastCharWasCarriageReturn = false
        case '\r' =>
          column = 0
          line += 1
          lastCharWasCarriageReturn = true
        case _ =>
          column += 1
          lastCharWasCarriageReturn = false
      }

      if (line == startLine) {
        if (column < startColumn) builder.append(" ") else builder.append(char)
      } else if (line > startLine) {
        builder.append(char)
      } else if (line > endLine || (line == endLine && column >= endColumn)) {
        return builder.mkString
      }
    }

    builder.mkString
  }

  override def toString: String = s"$startLine,$startColumn:$endLine,$endColumn"
}

object SourceSpan {

  /**
    * @return b if it contains a lower point than a, otherwise a
    */
  def lowest[T <: SourceSpan](a: T, b: T): T = {
    val aIsLowest = a.startLine < b.startLine || (a.startLine == b.startLine && a.startColumn <= b.startColumn)
    if (aIsLowest) a else b
  }

  /**
    * @return b if it contains a higher point than a, otherwise a
    */
  def highest[T <: SourceSpan](a: T, b: T): T = {
    val aIsHighest = a.startLine > b.startLine || (a.startLine == b.startLine && a.startColumn >= b.startColumn)
    if (aIsHighest) a else b
  }
}
