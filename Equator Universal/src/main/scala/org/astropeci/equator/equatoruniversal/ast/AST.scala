package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal.SourceSpan

/**
  * An abstract syntax tree for Equator source code.
  */
trait AST {

  def map[B <: AST](f: AST => B): B = f(mapChildren(_.map(f)))

  protected def mapChildren(f: AST => AST): AST

  def foreach(f: AST => Unit): Unit = children.foreach {
    child =>
      f(child)
      child.foreach(f)
  }

  def children: Seq[AST]

  def sourceSpan: Option[SourceSpan]

  def toSourceCode: String = toSourceCode(true)

  def toSourceCode(compact: Boolean): String

  def toCoreAST: CoreAST
}
