package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal._
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents a symbolic prefix application.
  */
case class PrefixApplicationAST(identifier: String, argument: AST,
                                override val sourceSpan: Option[SourceSpan]) extends AST {

  override protected def mapChildren(f: AST => AST): AST = PrefixApplicationAST(identifier, f(argument), sourceSpan)

  override def children: Seq[AST] = Vector(argument)

  override def toSourceCode(compact: Boolean): String = if (compact) {
    s"$identifier(${argument.toSourceCode(true)})"
  } else {
    val argumentStr = StringUtil.indent(argument.toSourceCode(false))
    s"$identifier (\n$argumentStr\n)"
  }

  override def toCoreAST: CoreAST = ApplicationAST(IdentifierAST.ofPrefix(identifier, sourceSpan), argument, sourceSpan)
}
