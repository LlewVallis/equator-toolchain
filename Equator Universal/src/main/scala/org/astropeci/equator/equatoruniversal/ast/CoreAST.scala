package org.astropeci.equator.equatoruniversal.ast

/**
  * An AST that represents a core Equator language feature.
  */
abstract class CoreAST extends AST {

  override def toCoreAST: CoreAST = this
}