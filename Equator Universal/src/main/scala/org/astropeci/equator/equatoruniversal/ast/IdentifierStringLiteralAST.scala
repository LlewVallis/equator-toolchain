package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal.SourceSpan

/**
  * An AST that represents an identifier string literal.
  */
case class IdentifierStringLiteralAST(content: String, override val sourceSpan: Option[SourceSpan]) extends AST {

  override def mapChildren(f: AST => AST): AST = this

  override def children: Seq[AST] = Vector()

  override def toSourceCode(compact: Boolean): String = '`' + content

  override def toCoreAST: CoreAST = StringLiteralAST(content, sourceSpan)
}
