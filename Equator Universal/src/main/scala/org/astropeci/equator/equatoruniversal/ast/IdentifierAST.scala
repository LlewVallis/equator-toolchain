package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal.SourceSpan

/**
  * An AST that represents an identifier.
  */
case class IdentifierAST(identifier: String, override val sourceSpan: Option[SourceSpan]) extends CoreAST {

  import IdentifierAST._

  override def mapChildren(f: AST => AST): AST = this

  override def children: Seq[AST] = Vector()

  override def toSourceCode(compact: Boolean): String = identifier

  def isStandard: Boolean = identifier forall StandardIdentifierWhitelist.contains

  def isSymbolic: Boolean = identifier forall SymbolicIdentifierWhitelist.contains

  /** The precedence of the symbolic identifier this represents. */
  def symbolicPrecedence: Int = {
    if (!isSymbolic) {
      throw new IllegalStateException("not a symbolic identifier")
    }

    identifier match {
      case id if id.startsWith("=") => 0
      case id if id.contains("?") => 1
      case id if id.contains(":") => 2
      case id if id.contains("!") || id.contains("$") => 3
      case id if id.contains("~") => 4
      case id if id.contains("|") => 5
      case id if id.contains("&") => 6
      case id if id.contains("=") => 7
      case id if id.contains("<") || id.contains(">") => 8
      case id if id.contains("+") || id.contains("-") => 9
      case id if id.contains("*") || id.contains("/") || id.contains("%") || id.contains("\\") => 10
      case id if id.contains("^") => 11
    }
  }
}

object IdentifierAST {

  private val StandardIdentifierWhitelist = (('a' to 'z') :+ ('A' to 'Z') :+ ('0' to '9')).toVector

  private val SymbolicIdentifierWhitelist =
    List('?', ':', '!', '$', '~', '|', '&', '=', '<', '>', '+', '-', '*', '/', '%', '\\', '^')

  def ofInfix(symbol: String, sourceSpan: Option[SourceSpan]): IdentifierAST =
    IdentifierAST(s"infix_$symbol", sourceSpan)

  def ofPrefix(symbol: String, sourceSpan: Option[SourceSpan]): IdentifierAST =
    IdentifierAST(s"prefix_$symbol", sourceSpan)

  def ofPostfix(symbol: String, sourceSpan: Option[SourceSpan]): IdentifierAST =
    IdentifierAST(s"postfix_$symbol", sourceSpan)
}