package org.astropeci.equator.equatoruniversal.token

/**
  * The type of a token.
  */
sealed trait TokenType

object TokenType {

  case object StandardIdentifier extends TokenType

  case object CharLiteral extends TokenType

  case object IdentifierStringLiteral extends TokenType

  case object DotNotationStringLiteral extends TokenType

  case object SymbolicIdentifier extends TokenType

  case object Comma extends TokenType

  case object At extends TokenType

  case object Semicolon extends TokenType

  case object WhitespaceCharLevel1 extends TokenType

  case object WhitespaceCharLevel2 extends TokenType

  case object LeftParen extends TokenType

  case object RightParen extends TokenType

  case object LeftCurly extends TokenType

  case object RightCurly extends TokenType

  case object LeftSquare extends TokenType

  case object RightSquare extends TokenType

  case object Unmatched extends TokenType

  // Standard string literals.

  case object StandardStringLiteralOpen extends TokenType

  case object StringLiteralEscapeBackspace extends TokenType

  case object StringLiteralEscapeTab extends TokenType

  case object StringLiteralEscapeNewline extends TokenType

  case object StringLiteralEscapeFormfeed extends TokenType

  case object StringLiteralEscapeCarriageReturn extends TokenType

  case object StringLiteralEscapeDoubleQuote extends TokenType

  case object StringLiteralEscapeApostrophe extends TokenType

  case object StringLiteralEscapeBackslash extends TokenType

  case object StandardStringLiteralContent extends TokenType

  case object StandardStringLiteralClose extends TokenType
}
