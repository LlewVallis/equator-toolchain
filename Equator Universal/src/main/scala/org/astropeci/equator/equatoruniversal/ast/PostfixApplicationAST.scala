package org.astropeci.equator.equatoruniversal.ast

import org.astropeci.equator.equatoruniversal._
import org.astropeci.equator.equatoruniversal.util.StringUtil

/**
  * An AST that represents a symbolic postfix application.
  */
case class PostfixApplicationAST(argument: AST, identifier: String,
                                 override val sourceSpan: Option[SourceSpan]) extends AST {

  override protected def mapChildren(f: AST => AST): AST = PostfixApplicationAST(f(argument), identifier, sourceSpan)

  override def children: Seq[AST] = Vector(argument)

  override def toSourceCode(compact: Boolean): String = if (compact) {
    s"(${argument.toSourceCode(true)})$identifier"
  } else {
    val argumentStr = StringUtil.indent(argument.toSourceCode(false))
    s"(\n$argumentStr\n) $identifier"
  }

  override def toCoreAST: CoreAST =
    ApplicationAST(IdentifierAST.ofPostfix(identifier, sourceSpan), argument, sourceSpan)
}
