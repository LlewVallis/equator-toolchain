scalaVersion := "2.12.6"
organization := "org.astropeci.equator"
name := "equator-universal"
version := "1.0"

// Remove postfix scala versions on artifacts.
crossPaths := false