scalaVersion := "2.12.6"
organization := "org.astropeci.equator"
name := "equator-parser"
version := "1.0"

// Remove postfix scala versions on artifacts.
crossPaths := false

enablePlugins(Antlr4Plugin)

antlr4PackageName in Antlr4 := Some("org.astropeci.equator.equatorparser.generated")

libraryDependencies += "org.antlr" % "antlr4-runtime" % "4.7.1"

libraryDependencies += "org.astropeci.equator" % "equator-universal" % "1.0"