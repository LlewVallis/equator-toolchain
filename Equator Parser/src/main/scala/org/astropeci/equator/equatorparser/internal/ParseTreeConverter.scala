package org.astropeci.equator.equatorparser.internal

import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ParseTree
import org.astropeci.equator.equatorparser.generated.UngroupedEquatorParser._
import org.astropeci.equator.equatoruniversal._

import scala.collection.JavaConverters._
import scala.language.reflectiveCalls

/**
  * Converts parse trees to ASTs.
  */
object ParseTreeConverter extends (ParseTree => AST) {

  private class Impl {

    /*
     * This implementation works with the concepts of consumers and listeners.
     *
     * A listener runs whenever a parse tree node enters. The implementation iterates through all nodes and runs
     * applicable listeners on them upon entering.
     *
     * A consumer is a function that accepts an AST and returns nothing. The consumer stack is a stack of consumers.
     * Consumers may be added to the top of the stack. ASTs may be pushed to the stack, when this happens the AST is
     * applied to the top of the stack and the consumer is popped.
     *
     * Using a combination of consuming and pushing this implementation builds ASTs.
     */

    private var consumers: Seq[AST => Unit] = Vector()

    private def consume(consumer: AST => Unit): Unit = consumers = consumer +: consumers

    private def push(ast: AST): Unit = {
      val consumer = consumers.head
      consumers = consumers.tail
      consumer(ast)
    }

    private def process(tree: ParseTree): Unit = {
      if (listener.isDefinedAt(tree)) listener(tree)

      for (i <- 0 until tree.getChildCount) {
        process(tree.getChild(i))
      }
    }

    def convert(tree: ParseTree): AST = {
      var ast: Option[AST] = None
      consume(consumed => ast = Some(consumed))

      process(tree)

      ast.get.map {
        case x: PostProcessedAST => x.processed
        case x => x
      }
    }

    private val listener: PartialFunction[ParseTree, Unit] = {
      // Standard functions.
      case ctx: FunctionContext => consume(body => push(FunctionAST(body, Some(contextToSourceSpan(ctx)))))

      // Lists.
      case ctx: ListContext =>
        val elemCount = ctx.elements.size
        var elems = Vector[AST]()

        def consumer(elem: AST): Unit = {
          elems :+= elem

          pushConsumerIfShould()
        }

        def pushConsumerIfShould(): Unit = {
          if (elems.size < elemCount) {
            consume(consumer)
          } else {
            push(ListAST(elems, Some(contextToSourceSpan(ctx))))
          }
        }

        pushConsumerIfShould()

      // Standard identifiers.
      case ctx: Standard_identifierContext =>
        push(new IdentifierAST(ctx.getText, Some(contextToSourceSpan(ctx))))

      // Symbolic identifiers.
      case ctx: Symbolic_identifierContext =>
        push(new IdentifierAST(ctx.getText, Some(contextToSourceSpan(ctx))))

      // Standard string literals.
      case ctx: String_literalContext =>
        push(StringLiteralAST(ctx.content.asScala.map(_.getText).mkString, Some(contextToSourceSpan(ctx))))

      // Identifier string literals.
      case ctx: Identifier_string_literalContext =>
        push(IdentifierStringLiteralAST(ctx.getText.tail, Some(contextToSourceSpan(ctx))))

      // Dot notation string literals.
      case ctx: Dot_notation_string_literalContext =>
        push(DotNotationStringLiteralAST(ctx.getText, Some(contextToSourceSpan(ctx))))

      // Argument applications.
      case ctx: Argument_applied_expressionContext => consume(applicant =>
        push(ArgumentApplicationAST(applicant, Some(contextToSourceSpan(ctx)))))

      // Non-terminated functions.
      case ctx: Non_terminated_functionContext => consume(body =>
        push(NonTerminatedFunctionAST(body, Some(contextToSourceSpan(ctx)))))

      // Expressions.
      case ctx: ExpressionContext =>
        val hasStandardExpr = ctx.standardExpr != null
        val hasConsumingExpr = ctx.consumingExpr != null

        if (hasStandardExpr && hasConsumingExpr) {
          consume(standard => {
            consume(consuming => {
              push(ApplicationAST(standard, consuming, Some(contextToSourceSpan(ctx))))
            })
          })
        }

        // If it is only one of a standard expression or a consuming expression, don't transform it's value.

      // Expressions, no consumers.
      case ctx: Expression_no_consumingContext => expressionEntryListener(ctx.expressions.size, ctx)

      // Expressions, no consumers, no newlines.
      case ctx: Expression_no_consuming_whitespace_0_or_1Context => expressionEntryListener(ctx.expressions.size, ctx)

      // Expressions, no consumers, no newlines, no spaces.
      case ctx: Expression_no_consuming_whitespace_0Context => expressionEntryListener(ctx.expressions.size, ctx)
    }

    private def expressionEntryListener(exprCount: Int, ctx: ParserRuleContext): Unit = {
      var exprs = Vector[AST]()

      def consumer(expr: AST): Unit = {
        exprs :+= expr

        if (exprs.size < exprCount) {
          consume(consumer)
        } else {
          push(ApplicationSequenceAST(exprs, Some(contextToSourceSpan(ctx))))
        }
      }

      consume(consumer)
    }

    private def contextToSourceSpan(ctx: ParserRuleContext): SourceSpan =
      AntlrTokens.sourceSpan(ctx.getStart) | AntlrTokens.sourceSpan(ctx.getStop)
  }

  def apply(parseTree: ParseTree): AST = new Impl().convert(parseTree)
}
