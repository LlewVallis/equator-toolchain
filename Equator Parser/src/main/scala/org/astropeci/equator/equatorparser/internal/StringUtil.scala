package org.astropeci.equator.equatorparser.internal

/**
  *
  */
object StringUtil {

  def escape(str: String): String = str.toSeq match {
    case Seq() => ""
    case first +: rest =>
      val newFirst = first match {
        case '\b' => "\\b"
        case '\t' => "\\t"
        case '\n' => "\\n"
        case '\f' => "\\f"
        case '\r' => "\\r"
        case '\"' => "\\\""
        case '\\' => "\\\\"

        case char => char.toString
      }

      newFirst + escape(rest.mkString)
  }

  def indent(str: String): String = {
    val builder = new StringBuilder(" ")

    str.zipWithIndex.foreach {
      case ('\n', _) => builder.append("\n ")
      case ('\r', index) if !(str.isDefinedAt(index + 1) && str(index + 1) == '\n') => builder.append("\r ")
      case (char, _) => builder.append(char)
    }

    builder.toString
  }
}
