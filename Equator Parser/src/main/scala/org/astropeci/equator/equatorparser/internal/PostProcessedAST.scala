package org.astropeci.equator.equatorparser.internal

import org.astropeci.equator.equatoruniversal.AST

/**
  * An AST that must have additional processing applied after parsing.
  */
trait PostProcessedAST extends AST {

  def processed: AST
}
