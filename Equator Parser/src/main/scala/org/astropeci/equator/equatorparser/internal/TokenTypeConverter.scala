package org.astropeci.equator.equatorparser.internal

import org.astropeci.equator.equatoruniversal.TokenType

/**
  * Converts numeric Antlr token types to token types exposed by this library.
  */
object TokenTypeConverter extends (Int => TokenType) {

  override def apply(typeNumber: Int): TokenType = {
    import org.astropeci.equator.equatorparser.generated.UngroupedEquatorLexer._
    import org.astropeci.equator.equatoruniversal.TokenType._

    typeNumber match {
      case STANDARD_IDENTIFIER => StandardIdentifier
      case IDENTIFIER_STRING_LITERAL => IdentifierStringLiteral
      case DOT_NOTATION_STRING_LITERAL => DotNotationStringLiteral
      case SYMBOLIC_IDENTIFIER => SymbolicIdentifier
      case COMMA => Comma
      case AT => At
      case SEMICOLON => Semicolon

      case WHITESPACE_LVL_1_CHAR => WhitespaceCharLevel1
      case WHITESPACE_LVL_2_CHAR => WhitespaceCharLevel2

      case LPAREN => LeftParen
      case RPAREN => RightParen
      case LCURLY => LeftCurly
      case RCURLY => RightCurly
      case LSQUARE => LeftSquare
      case RSQUARE => RightSquare

      case UNMATCHED => Unmatched

      case STRING_LITERAL_OPEN => StandardStringLiteralOpen
      case ESCAPE_BACKSPACE => StringLiteralEscapeBackspace
      case ESCAPE_TAB => StringLiteralEscapeTab
      case ESCAPE_NEWLINE => StringLiteralEscapeNewline
      case ESCAPE_FORMFEED => StringLiteralEscapeFormfeed
      case ESCAPE_CARRIAGE_RETURN => StringLiteralEscapeCarriageReturn
      case ESCAPE_DOUBLE_QUOTE => StringLiteralEscapeDoubleQuote
      case ESCAPE_APOSTROPHE => StringLiteralEscapeApostrophe
      case ESCAPE_BACKSLASH => StringLiteralEscapeBackslash
      case STRING_CONTENT => StandardStringLiteralContent
      case STRING_LITERAL_CLOSE => StandardStringLiteralClose
    }
  }
}
