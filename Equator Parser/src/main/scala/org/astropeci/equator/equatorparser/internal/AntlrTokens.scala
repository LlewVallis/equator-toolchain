package org.astropeci.equator.equatorparser.internal

import org.antlr.v4.runtime.{Token => AntlrToken}
import org.astropeci.equator.equatoruniversal.SourceSpan

/**
  * A utility object for dealing with Antlr tokens.
  */
object AntlrTokens {

  def sourceSpan(token: AntlrToken): SourceSpan = {
    // Use simplified newlines.
    val simplifiedText = token.getText.replace("\r\n", "\n").replace('\r', '\n')

    // Antlr 1 indexes lines.
    val startLine = token.getLine
    val startColumn = token.getCharPositionInLine

    var endLine = startLine
    var endColumn = startColumn

    for (char <- simplifiedText) {
      if (char == '\n') {
        endLine += 1
        endColumn = 0
      } else {
        endColumn += 1
      }
    }

    SourceSpan(startLine, startColumn, endLine, endColumn)
  }
}
