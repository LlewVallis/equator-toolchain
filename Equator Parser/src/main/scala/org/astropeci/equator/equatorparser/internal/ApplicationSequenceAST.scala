package org.astropeci.equator.equatorparser.internal

import org.astropeci.equator.equatoruniversal._

/**
  * An AST that represents an application sequence.
  */
case class ApplicationSequenceAST(elements: Seq[AST],
                                  override val sourceSpan: Option[SourceSpan]) extends PostProcessedAST {

  if (elements.isEmpty) throw new IllegalArgumentException("no elements")

  override def mapChildren(f: AST => AST): AST = ApplicationSequenceAST(elements.map(f), sourceSpan)

  override def children: Seq[AST] = elements

  override def toString: String = productPrefix + elements.mkString("(", ",", ")")

  override def toSourceCode(compact: Boolean): String = if (compact) {
    elements.map(_.toSourceCode(true)).mkString("(", ")(", ")")
  } else {
    elements.map(elem => StringUtil.indent(elem.toSourceCode(false))).mkString("(\n", "\n) (\n", "\n)")
  }

  override def processed: AST = simplifySequence(elements)

  private def simplifySequence(elements: Seq[AST]): AST = {
    lowestPrecedenceInfixIndex(elements) match {
      // Single element sequence.
      case _ if elements.size == 1 => elements.head

      // Lowest precedence operator at start of sequence, prefix.
      case Some(0) if elements.size >= 2 => elements match {
        case (head: IdentifierAST) +: tail => mkPrefix(head, simplifySequence(tail))
      }

      // Lowest precedence operator at end of sequence, postifx.
      case Some(index) if index == elements.indices.last => elements match {
        case init :+ (last: IdentifierAST) =>
          mkPostfix(simplifySequence(init), last)
      }

      // Lowest precedence operator somewhere else, infix.
      case Some(index) =>
        val left = simplifySequence(elements.take(index))
        val id = elements(index).asInstanceOf[IdentifierAST]
        val right = simplifySequence(elements.drop(index + 1))
        mkInfix(left, id, right)

      // No symbolic identifier and two elements in sequence, simple application.
      case None if elements.size == 2 => mkApplication(elements.head, elements.last)

      // No symbolic identifier, chain of applications.
      case None => mkApplication(simplifySequence(elements.init), elements.last)
    }
  }

  private def lowestPrecedenceInfixIndex(elements: Seq[AST]): Option[Int] = {
    // Precedence and index of lowest precedence symbolic identifier.
    var lowestPrecedenceSymbolicIdentifier: Option[(Int, Int)] = None

    for ((elem: IdentifierAST, index) <- elements.zipWithIndex if elem.isSymbolic) {
      lowestPrecedenceSymbolicIdentifier match {
        // No symbolic identifier found yet.
        case None => lowestPrecedenceSymbolicIdentifier = Some(elem.symbolicPrecedence, index)

        // There is a previous one, but this one has lower precedence.
        case Some((otherPrecedence, _)) if elem.symbolicPrecedence < otherPrecedence =>
          lowestPrecedenceSymbolicIdentifier = Some(elem.symbolicPrecedence, index)
      }
    }

    lowestPrecedenceSymbolicIdentifier.map(_._2)
  }

  private def mkPrefix(id: IdentifierAST, body: AST): AST =
    PrefixApplicationAST(id.identifier, body, orSourceSpanOptions(id.sourceSpan, body.sourceSpan))

  private def mkPostfix(body: AST, id: IdentifierAST): AST =
    PostfixApplicationAST(body, id.identifier, orSourceSpanOptions(body.sourceSpan, id.sourceSpan))

  private def mkInfix(left: AST, id: IdentifierAST, right: AST): AST =
    InfixApplicationAST(left, id.identifier, right,
      orSourceSpanOptions(left.sourceSpan, id.sourceSpan, right.sourceSpan))

  private def mkApplication(left: AST, right: AST): AST =
    ApplicationAST(left, right, orSourceSpanOptions(left.sourceSpan, right.sourceSpan))

  private def orSourceSpanOptions(a: Option[SourceSpan], b: Option[SourceSpan]): Option[SourceSpan] = (a, b) match {
    case (Some(aSourceSpan), Some(bSourceSpan)) => Some(aSourceSpan | bSourceSpan)
    case (Some(aSourceSpan), None) => Some(aSourceSpan)
    case (None, Some(bSourceSpan)) => Some(bSourceSpan)
    case (None, None) => None
  }

  private def orSourceSpanOptions(a: Option[SourceSpan], b: Option[SourceSpan],
                                  c: Option[SourceSpan]): Option[SourceSpan] =
    orSourceSpanOptions(a, orSourceSpanOptions(b, c))

  override def toCoreAST: CoreAST = processed.toCoreAST
}
