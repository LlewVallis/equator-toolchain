package org.astropeci.equator.equatorparser

import java.nio.IntBuffer

import org.antlr.v4.runtime.misc.ParseCancellationException
import org.antlr.v4.runtime.{Token => AntlrToken, _}
import org.astropeci.equator.equatorparser.generated.{UngroupedEquatorLexer, UngroupedEquatorParser}
import org.astropeci.equator.equatorparser.internal.{AntlrTokens, CancelingErrorListener, ParseTreeConverter, TokenTypeConverter}
import org.astropeci.equator.equatoruniversal.{AST, Token}

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

/**
  * A parser and lexer for the Equator programming langage.
  */
object EquatorParser {

  def main(args: Array[String]): Unit = {
    val res = parse("""a""")
    println(res.get)
    println()
    println(res.get.toSourceCode(false))
  }

  def parse(codePoints: Seq[Int]): Try[AST] = {
    val lexer = lexerForCodePoints(codePoints)
    val tokenStream = new CommonTokenStream(lexer)

    val parser = new UngroupedEquatorParser(tokenStream)
    parser.removeErrorListeners()
    parser.addErrorListener(new CancelingErrorListener)

    try {
      val parseTree = parser.program()
      parseTree.exception match {
        case null => Success(ParseTreeConverter(parseTree))
        case e => Failure(new ParseException(e.getMessage))
      }
    } catch {
      case e: ParseCancellationException => Failure(new ParseException(e.getMessage))
    }
  }

  def parse(program: String): Try[AST] = parse(stringToCodePoints(program))

  def lex(codePoints: Seq[Int]): Seq[Token] = {
    val lexer = lexerForCodePoints(codePoints)

    var tokens = Vector[Token]()

    var nextToken = lexer.nextToken()
    while (nextToken.getType != AntlrToken.EOF) {
      val sourceSpan = AntlrTokens.sourceSpan(nextToken)
      val token = Token(TokenTypeConverter(nextToken.getType), nextToken.getText, sourceSpan)
      tokens :+= token

      nextToken = lexer.nextToken()
    }

    tokens
  }

  def lex(program: String): Seq[Token] = lex(stringToCodePoints(program))

  private def stringToCodePoints(str: String): Seq[Int] = str.codePoints.iterator.asScala.map(i => i: Int).toSeq

  private def codePointsToStream(codePoints: Seq[Int]): CharStream = {
    val buffer = IntBuffer.allocate(codePoints.size)
    codePoints.foreach(buffer.put)
    buffer.position(0)
    CodePointCharStream.fromBuffer(CodePointBuffer.withInts(buffer))
  }

  private def lexerForCodePoints(codePoints: Seq[Int]) = new UngroupedEquatorLexer(codePointsToStream(codePoints))
}