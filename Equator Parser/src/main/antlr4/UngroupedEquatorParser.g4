parser grammar UngroupedEquatorParser;

options {
    tokenVocab = UngroupedEquatorLexer;
}

file : WHITESPACE? (normal_file | externed_file) WHITESPACE? EOF ;

normal_file : (statement (WHITESPACE? statement)*)? ;

/*

type Monad Type {
    let map: NewType => (Type -> NewType) -> Self NewType
    let flatMap: NewType
}

*/

externed_file :
    EXTERNED WHITESPACE
    (
          internal_definition
        | internal_outer_proof
    )
    ;

statement :
      definition
    | outer_proof
    //| typeDef
    //| implicit
    ;

outer_proof : external_outer_proof | internal_outer_proof ;

external_outer_proof : EXTERN WHITESPACE PROVE WHITESPACE? string_literal WHITESPACE? SEMICOLON ;

internal_outer_proof :
    PROVE WHITESPACE type WHITESPACE FOR WHITESPACE type
    (WHITESPACE standard_identifier)? WHITESPACE? proof_block
    WHITESPACE? SEMICOLON
    ;

proof_block : LCURLY WHITESPACE? (definition WHITESPACE?)* RCURLY ;

definition : external_definition | internal_definition ;

external_definition : EXTERN WHITESPACE LET WHITESPACE? string_literal WHITESPACE? SEMICOLON ;

internal_definition :
    LET WHITESPACE
    definition_identifier WHITESPACE?
    (COLON WHITESPACE? type WHITESPACE?)?
    EQUALS WHITESPACE?
    simple_expression WHITESPACE?
    SEMICOLON
    ;

definition_identifier : standard_identifier | symbolic_identifier_reference ;

extended_expression : (statement WHITESPACE?)* simple_expression ;

simple_expression :
    simple_expression_no_infix (WHITESPACE? symbolic_identifier WHITESPACE? simple_expression_no_infix)*
    ;

simple_expression_no_infix :
    (simple_expression_no_infix_no_app_no_fun WHITESPACE?)* simple_expression_no_infix_no_app
    ;

simple_expression_no_infix_no_app : simple_expression_no_infix_no_app_no_fun | function ;

simple_expression_no_infix_no_app_no_fun :
      number
    | char_literal
    | string_literal
    | list
    | standard_identifier
    | symbolic_identifier_reference
    | LPAREN WHITESPACE? extended_expression WHITESPACE? RPAREN
    ;

number : NUMBER ;

list :
    LSQUARE WHITESPACE?
    (
        simple_expression WHITESPACE? (COMMA WHITESPACE? simple_expression WHITESPACE?)*
    )?
    RSQUARE
    ;

char_literal : CHAR_LITERAL ;

string_literal :
    STRING_LITERAL_OPEN
    (
          STRING_CONTENT
        | ESCAPE_BACKSPACE
        | ESCAPE_TAB
        | ESCAPE_NEWLINE
        | ESCAPE_FORMFEED
        | ESCAPE_CARRIAGE_RETURN
        | ESCAPE_DOUBLE_QUOTE
        | ESCAPE_BACKSLASH
    )*
    STRING_LITERAL_CLOSE
    ;

function :
      bracketed_function
    | unbracketed_function
    ;

bracketed_function : LCURLY WHITESPACE? (unbracketed_function | extended_expression) WHITESPACE? RCURLY ;

unbracketed_function :
    (type_parameter WHITESPACE?)*
    (implicit_parameter WHITESPACE?)*
    standard_parameter WHITESPACE?
    extended_expression
    ;

type_parameter :
    type_identifier WHITESPACE?
    (COLON WHITESPACE? type WHITESPACE?)?
    THICK_ARROW
    ;

implicit_parameter :
    standard_identifier WHITESPACE?
    (COLON WHITESPACE? type WHITESPACE?)?
    TWIST_ARROW
    ;

standard_parameter :
    standard_identifier WHITESPACE?
    (COLON WHITESPACE? type WHITESPACE?)?
    THIN_ARROW
    ;

symbolic_identifier_reference :
    LPAREN WHITESPACE?
    (symbolic_identifier | symbolic_identifier_reference) WHITESPACE?
    RPAREN
    ;

type : TYPE_IDENTIFIER ;

standard_identifier : STANDARD_IDENTIFIER ;

symbolic_identifier : SYMBOLIC_IDENTIFIER ;

type_identifier : TYPE_IDENTIFIER ;

/*
program : WHITESPACE? form WHITESPACE? EOF ;

form : (statement WHITESPACE*) expression ;

statement :
      assignment
    | proof
    ;

assignment : LET identifier EQUALS expression SEMICOLON ;

expression :
      (
          standardExpr = expression_no_consuming
          consumingExpr = consuming?
      )
    | consumingExpr = consuming
    ;

expression_no_consuming :
    expressions += expression_no_consuming_WHITESPACE_0_or_1
    (
        WHITESPACE_lvl_2
        expressions += expression_no_consuming_WHITESPACE_0_or_1
    )*
    ;

expression_no_consuming_WHITESPACE_0_or_1 :
    expressions += expression_no_consuming_WHITESPACE_0
    (
        WHITESPACE_lvl_1
        expressions += expression_no_consuming_WHITESPACE_0
    )*
    ;

expression_no_consuming_WHITESPACE_0 : expressions += atom+ ;

atom :
      parenthesised_expression
    | function
    | list
    | standard_identifier
    | symbolic_identifier
    | string_literal
    | identifier_string_literal
    | dot_notation_string_literal
    ;

consuming :
      argument_applied_expression
    | non_terminated_function
    ;

list :
      LSQUARE WHITESPACE? RSQUARE
    | (
          LSQUARE
          WHITESPACE? elements += expression WHITESPACE?
          (COMMA WHITESPACE? elements += expression WHITESPACE?)*
          RSQUARE
      )
    ;

argument_applied_expression : AT WHITESPACE? expression ;

non_terminated_function : SEMICOLON WHITESPACE? expression ;

parenthesised_expression : LPAREN WHITESPACE? expression WHITESPACE? RPAREN ;

function : LCURLY WHITESPACE? expression WHITESPACE? RCURLY ;

identifier : standard_identifier | symbolic_identifier ;

standard_identifier : STANDARD_IDENTIFIER ;

symbolic_identifier : SYMBOLIC_IDENTIFIER ;

string_literal :
    STRING_LITERAL_OPEN
    content += (
          STRING_CONTENT
        | ESCAPE_BACKSPACE
        | ESCAPE_TAB
        | ESCAPE_NEWLINE
        | ESCAPE_FORMFEED
        | ESCAPE_CARRIAGE_RETURN
        | ESCAPE_DOUBLE_QUOTE
        | ESCAPE_APOSTROPHE
        | ESCAPE_BACKSLASH
    )*
    STRING_LITERAL_CLOSE;

WHITESPACE : (WHITESPACE_LVL_1_CHAR | WHITESPACE_LVL_2_CHAR)+ ;

WHITESPACE_lvl_1 : WHITESPACE_LVL_1_CHAR+ ;

WHITESPACE_lvl_2 : WHITESPACE_LVL_1_CHAR* (WHITESPACE_LVL_2_CHAR WHITESPACE_LVL_1_CHAR*)+ ;
*/