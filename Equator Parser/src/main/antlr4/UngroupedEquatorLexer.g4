lexer grammar UngroupedEquatorLexer;

LET : 'let' ;

PROVE : 'prove' ;

FOR : 'for' ;

EXTERN : 'extern' ;

EXTERNED : 'externed' ;

EQUALS : '=' ;

COLON : ':' ;

COMMA : ',' ;

SEMICOLON : ';' ;

THIN_ARROW : '->' ;

TWIST_ARROW : '~>' ;

THICK_ARROW : '=>' ;

UNDERSCORE : '_' ;

LPAREN : '(' ;

RPAREN : ')' ;

LCURLY : '{' ;

RCURLY : '}' ;

LSQUARE : '[' ;

RSQUARE : ']' ;

NUMBER :
      NUMBER_RADIX_2
    | NUMBER_RADIX_3
    | NUMBER_RADIX_4 
    | NUMBER_RADIX_5 
    | NUMBER_RADIX_6 
    | NUMBER_RADIX_7 
    | NUMBER_RADIX_8 
    | NUMBER_RADIX_9 
    | NUMBER_RADIX_10 
    | NUMBER_RADIX_11 
    | NUMBER_RADIX_12 
    | NUMBER_RADIX_13 
    | NUMBER_RADIX_14 
    | NUMBER_RADIX_15 
    | NUMBER_RADIX_16
    | NUMBER_NO_RADIX
    ;

fragment NUMBER_NO_RADIX : [0-9]+ ;
fragment NUMBER_RADIX_2 : '2x' [0-1]+ ;
fragment NUMBER_RADIX_3 : '3x' [0-2]+ ;
fragment NUMBER_RADIX_4 : '4x' [0-3]+ ;
fragment NUMBER_RADIX_5 : '5x' [0-4]+ ;
fragment NUMBER_RADIX_6 : '6x' [0-5]+ ;
fragment NUMBER_RADIX_7 : '7x' [0-6]+ ;
fragment NUMBER_RADIX_8 : '8x' [0-7]+ ;
fragment NUMBER_RADIX_9 : '9x' [0-8]+ ;
fragment NUMBER_RADIX_10 : '10x' [0-9]+ ;
fragment NUMBER_RADIX_11 : '11x' [0-9A]+ ;
fragment NUMBER_RADIX_12 : '12x' [0-9A-B]+ ;
fragment NUMBER_RADIX_13 : '13x' [0-9A-C]+ ;
fragment NUMBER_RADIX_14 : '14x' [0-9A-D]+ ;
fragment NUMBER_RADIX_15 : '15x' [0-9A-E]+ ;
fragment NUMBER_RADIX_16 : '16x' [0-9A-F]+ ;

STRING_LITERAL_OPEN : '"' -> pushMode(MODE_IN_STRING_LITERAL) ;

CHAR_LITERAL :
    '\''
    (~[\\'] | '\\b' | '\\t' | '\\n' | '\\f' | '\\r' | '\\\'' | '\\\\' )
    '\''
    ;

STANDARD_IDENTIFIER : [a-z][A-Za-z0-9_]* ;

SYMBOLIC_IDENTIFIER : [~^/\\*%+\-<>=&|!$:?]+ ;

TYPE_IDENTIFIER : [A-Z][A-Za-z0-9_]* ;

WHITESPACE : (' ' | '\t' | '\r\n' | '\r' | '\n')+ ;

UNMATCHED : . ;

mode MODE_IN_STRING_LITERAL ;

ESCAPE_BACKSPACE : '\\b' ;

ESCAPE_TAB : '\\t' ;

ESCAPE_NEWLINE : '\\n' ;

ESCAPE_FORMFEED : '\\f' ;

ESCAPE_CARRIAGE_RETURN : '\\r' ;

ESCAPE_DOUBLE_QUOTE : '\\"' ;

ESCAPE_BACKSLASH : '\\\\' ;

STRING_CONTENT : ~[\\"]+ ;

STRING_LITERAL_CLOSE : '"' -> popMode ;

STR_UNMATCHED : . -> type(UNMATCHED) ;